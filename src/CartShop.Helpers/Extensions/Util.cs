﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CartShop.Helpers.Extensions
{
    public static class Util
    {
        private const string AllowableCharacters = "abcdefghijklmnopqrstuvwxyz0123456789";
        public static string ComputeStringToHash(this string input, HashAlgorithm algorithm)
        {
            if (input == null) input = "Abc@123";
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);
            return BitConverter.ToString(hashedBytes);
        }
        
        public static string MD5Hash(this string input)
        {
            using (var md5 = MD5.Create())
            {
                var data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();
                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                // Return the hexadecimal string. 
                return sBuilder.ToString();
            }
        }

        public static string GenPassWord(this string password)
        {
            if (password == null) password = "Abc@123";
            return password.MD5Hash();
        }

        public static string GenTokenKey(this string Token)
        {
            if (Token == null) Token = "ngvannam";
            string timeLong = DateTime.Now.ToString();
            var sum = Token + timeLong;
            return sum.MD5Hash();
        }


        public static string GenCodeChangePass(string email, Guid UserId)
        {
            if (email == null) email = "nguyenvannam0411@gmail.com";
            if (UserId == null) UserId = Guid.NewGuid();
            var code = email + UserId;
            return code.MD5Hash();
        }

        public static string GenerateRandomString(int length)
        {
            var bytes = new byte[length];
            using (var random = RandomNumberGenerator.Create())
            {
                random.GetBytes(bytes);
            }
            return new string(bytes.Select(x => AllowableCharacters[x % AllowableCharacters.Length]).ToArray());
        }

        //convert tieng viet thanh khong dau va them dau -
        public static string unicodeToNoMark(this string input)
        {
            input = input.ToLowerInvariant().Trim();
            if (input == null) return "";
            string noMark = "a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,e,e,e,e,e,e,e,e,e,e,e,e,u,u,u,u,u,u,u,u,u,u,u,u,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,i,i,i,i,i,i,y,y,y,y,y,y,d,A,A,E,U,O,O,D";
            string unicode = "a,á,à,ả,ã,ạ,â,ấ,ầ,ẩ,ẫ,ậ,ă,ắ,ằ,ẳ,ẵ,ặ,e,é,è,ẻ,ẽ,ẹ,ê,ế,ề,ể,ễ,ệ,u,ú,ù,ủ,ũ,ụ,ư,ứ,ừ,ử,ữ,ự,o,ó,ò,ỏ,õ,ọ,ơ,ớ,ờ,ở,ỡ,ợ,ô,ố,ồ,ổ,ỗ,ộ,i,í,ì,ỉ,ĩ,ị,y,ý,ỳ,ỷ,ỹ,ỵ,đ,Â,Ă,Ê,Ư,Ơ,Ô,Đ";
            string[] a_n = noMark.Split(',');
            string[] a_u = unicode.Split(',');
            for (int i = 0; i < a_n.Length; i++)
            {
                input = input.Replace(a_u[i], a_n[i]);
            }
            input = input.Replace("  ", " ");
            input = Regex.Replace(input, "[^a-zA-Z0-9% ._]", string.Empty);
            input = removeSpecialChar(input);
            input = input.Replace(" ", "-");
            input = input.Replace("--", "-");
            return input;
        }
        public static string removeSpecialChar(this string input)
        {
            input = input.Replace("-", "").Replace(":", "").Replace(",", "").Replace("_", "").Replace("'", "").Replace("\"", "").Replace(";", "").Replace("”", "").Replace(".", "").Replace("%", "");
            return input;
        }

        public static DateTime ConvertToDatetime(string day)
        {
            if (day == null)
            {
                day = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            }
            DateTime dt;
            if (DateTime.TryParseExact(day, "d/M/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
            {
                return dt;
            }
            else
            {
                return DateTime.Now;
            }

        }

        public static int dateDiff(DateTime date1, DateTime date2)
        {
            try
            {
                if (date1 == null || date2 == null) return 0;
                TimeSpan TS = new System.TimeSpan(date1.Ticks - date2.Ticks);
                return (int)Math.Abs(TS.TotalDays);
            }
            catch (Exception ex) { return 100; }
        }

       
    }
}
