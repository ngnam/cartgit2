﻿using System.Text;
using System.Text.RegularExpressions;

namespace CartShop.Helpers.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Ensure an Uri must end without forward slash or start with forward slash
        /// </summary>
        /// <param name="input">Uri to check its starting or ending</param>
        /// <param name="checkEnding">True: check if the uri starts with forward slash. False: check if the uri ends without forward slash</param>
        /// <returns></returns>
        public static string NormalizeUri(this string input, bool checkEnding = true)
        {
            if (string.IsNullOrEmpty(input))
                return input;

            if (checkEnding)
            {
                string uriWithoutSlashEnding = !input.EndsWith(@"/")
                  ? $"{input}/"
                  : input;

                return uriWithoutSlashEnding;
            }
            else
            {
                string uriWithSlashStarting = input.StartsWith(@"/")
                  ? input.Substring(1)
                  : input;

                return uriWithSlashStarting;
            }
        }

        public static string EnCodingISO88591(string stringEncoding)
        {
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            byte[] isoBytes = iso.GetBytes(stringEncoding);
            string stringAfterEncoding = iso.GetString(isoBytes);
            return stringAfterEncoding;
        }

        /// <summary>
        /// Remove minute in TimeZone DisplayName
        /// Input:  (UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna
        /// Output: (UTC+01) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna
        /// </summary>
        public static string RemoveMinuteDisplayName(this string timeZoneInfoDisplayName, string strReplace = "")
        {
            if (string.IsNullOrWhiteSpace(timeZoneInfoDisplayName))
                return "";
            string pattern = "\\:[0-9][0-9]";
            Regex rgx = new Regex(pattern);
            return rgx.Replace(timeZoneInfoDisplayName, strReplace);
        }
    }
}