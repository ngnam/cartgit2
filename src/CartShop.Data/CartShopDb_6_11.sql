USE [CartShopDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/06/2017 00:40:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171102151423_InitialCreate', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171102173507_UpdateForeignKeyUserProduct', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171103033411_updateColumnNameForRoleEmp', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171105084254_AddOrderByTableMenu', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171105111156_AddIconTableMenu', N'1.1.0-rtm-22752')
/****** Object:  Table [dbo].[Menus]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[menu_id] [int] IDENTITY(1,1) NOT NULL,
	[level_user] [int] NULL,
	[menu_name] [nvarchar](250) NULL,
	[menu_url] [nvarchar](250) NULL,
	[parent_id] [int] NULL,
	[status] [bit] NULL,
	[order_by] [int] NULL,
	[icon] [nvarchar](max) NULL,
 CONSTRAINT [PrimaryKey_MenuId] PRIMARY KEY CLUSTERED 
(
	[menu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Menus] ON
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (2, 1, N'Quản lý nhân viên', N'/Account', NULL, 1, 4, N'fa-users')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (3, 1, N'Quản lý danh mục ', N'/Category', NULL, 1, 3, N'fa-list')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (5, 1, N'Quản lý hàng hóa', N'/Products', NULL, 1, 2, N'fa-shopping-bag')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (6, 1, N'Quản lý bán hàng', N'/Orders', NULL, 1, 1, N'fa-cart-plus')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (7, 1, N'Báo cáo', N'#', NULL, 1, 5, N'fa-book')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (8, 2, N'Báo cáo hàng tồn kho', N'/Report/TonKho', 7, 1, 1, N'fa-book')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (9, 2, N'Báo cáo doanh thu', N'/Report/DoanhThu', 7, 1, 2, N'fa-book')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (10, 1, N'Config', N'#', NULL, 1, 6, N'fa-cogs')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (11, 1, N'Quản lý menu', N'#', 10, 1, 1, N'fa-list')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (12, 1, N'Settings', N'#', 10, 1, 2, N'fa-cog')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (13, 1, N'Quản lý nhà cung cấp', N'#', 10, 1, 3, N'fa-fire')
SET IDENTITY_INSERT [dbo].[Menus] OFF
/****** Object:  Table [dbo].[Roles]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](250) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (N'fab4ccfd-6181-40bb-9314-0e45176d0845', N'User')
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', N'Employee')
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (N'430c85c3-99ed-4b48-94f4-a78a7084fee9', N'Administrator')
/****** Object:  Table [dbo].[Providers]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Providers](
	[provider_id] [int] IDENTITY(1,1) NOT NULL,
	[provider_address] [nvarchar](250) NULL,
	[provider_name] [nvarchar](250) NULL,
	[provider_note] [nvarchar](250) NULL,
	[provider_rating] [int] NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PrimaryKey_ProviderId] PRIMARY KEY CLUSTERED 
(
	[provider_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Providers] ON
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (1, NULL, N'Sony', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (2, NULL, N'SamSung', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (3, NULL, N'Apple', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (4, NULL, N'Oppo', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (5, NULL, N'Adidas', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (6, NULL, N'Bitis', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (7, NULL, N'Vinamik', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (8, NULL, N'Made in china', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (9, NULL, N'Không rõ nguồn gốc', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (10, NULL, N'Xách tay', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Providers] OFF
/****** Object:  Table [dbo].[Categories]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [nvarchar](250) NULL,
	[category_type] [int] NULL,
	[parent_cat_id] [int] NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PrimaryKey_CategoryId] PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (1, N'Điện tử & công nghệ', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (2, N'Gia dụng', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (3, N'Nội thất', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (4, N'Thời trang', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (5, N'Trang sức', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (6, N'Mẹ & bé', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (7, N'Sức khỏe & Làm đẹp', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (8, N'Du Lịch', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (9, N'Ô tô & xe máy', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Categories] OFF
/****** Object:  Table [dbo].[Users]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[user_id] [uniqueidentifier] NOT NULL,
	[change_pass] [int] NULL,
	[code_change_pass] [nvarchar](max) NULL,
	[date_created] [datetime2](7) NULL,
	[date_deleted] [datetime2](7) NULL,
	[date_updated] [datetime2](7) NULL,
	[email] [nvarchar](250) NULL,
	[email_confirm] [nvarchar](250) NULL,
	[login_status] [int] NULL,
	[pass_hash] [nvarchar](max) NULL,
	[pass_word] [nvarchar](250) NULL,
	[role_id] [uniqueidentifier] NOT NULL,
	[status] [bit] NOT NULL,
	[user_create] [uniqueidentifier] NULL,
	[user_name] [nvarchar](250) NULL,
 CONSTRAINT [PrimaryKey_UserId] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'14ecc699-cc6b-465c-87c0-462e9fdaf3f5', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste6@gmail.com', N'teste6@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee6')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'350ffd56-54db-4ce0-be51-466249106a73', NULL, NULL, CAST(0x07DF7051EC137D3D0B AS DateTime2), CAST(0x07A9109FA9177D3D0B AS DateTime2), NULL, N'sss@ff', N'sss@ff', 0, N'698d51a19d8a121ce581499d7b701668', N'111', N'fab4ccfd-6181-40bb-9314-0e45176d0845', 0, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'sss1')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'282b88bf-6523-46a5-a8d8-4b80e7e28e0b', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), CAST(0x07AE7F6A91177D3D0B AS DateTime2), CAST(0x072B2250B0127D3D0B AS DateTime2), N'teste4@gmail.com', N'teste4@gmail.com', NULL, N'c4ca4238a0b923820dcc509a6f75849b', N'1', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 0, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee4')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', NULL, NULL, CAST(0x073039F432067B3D0B AS DateTime2), NULL, CAST(0x077E3F9C7FC87D3D0B AS DateTime2), N'adminshop@cartshop.com', N'adminshop@cartshop.com', NULL, N'e10adc3949ba59abbe56e057f20f883e', N'123456', N'430c85c3-99ed-4b48-94f4-a78a7084fee9', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'admin')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'54d60694-b095-43e6-9390-6aa0cef88eb5', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste7@gmail.com', N'teste7@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee7')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'd7f9119b-1d6f-4b2f-90b8-6ec4cf1310d6', NULL, NULL, CAST(0x07D07D11C2067B3D0B AS DateTime2), NULL, NULL, N'teste2@gmail.com', N'teste2@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee3')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'860325c3-4b93-4e9b-b1bc-7419d18c8ebb', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste8@gmail.com', N'teste8@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee8')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'2f990f30-fe1b-4d98-9d87-aaec98ea9766', NULL, NULL, CAST(0x07A4E366E4C17C3D0B AS DateTime2), NULL, CAST(0x07D19C20870D7D3D0B AS DateTime2), N'nam22@gmail.com', N'nam22@gmail.com', 0, N'698d51a19d8a121ce581499d7b701668', N'111', N'fab4ccfd-6181-40bb-9314-0e45176d0845', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'nam22')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'5f2af64d-b1a5-42e7-aaef-dc0f2f2112dd', NULL, NULL, CAST(0x07D07D11C2067B3D0B AS DateTime2), NULL, NULL, N'teste1@gmail.com', N'teste1@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee1')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'7a53e1b8-003d-46b0-8c53-f0104d09ce23', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste5@gmail.com', N'teste5@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee5')
/****** Object:  Table [dbo].[Employees]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[user_id] [uniqueidentifier] NOT NULL,
	[address] [nvarchar](250) NULL,
	[genner] [nvarchar](250) NULL,
	[employee_name] [nvarchar](250) NULL,
	[office] [int] NULL,
	[phone] [nvarchar](11) NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'14ecc699-cc6b-465c-87c0-462e9fdaf3f5', N'Hanoi', NULL, N'employee 1', 2, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'350ffd56-54db-4ce0-be51-466249106a73', N'111', N'1', N'111', 2, N'111')
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'282b88bf-6523-46a5-a8d8-4b80e7e28e0b', N'Hanoi', N'1', N'employee 2', 3, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'Hanoi', N'1', N'Nguyễn Văn Nam', 3, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'd7f9119b-1d6f-4b2f-90b8-6ec4cf1310d6', N'Hanoi', NULL, N'employee 4', 2, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'860325c3-4b93-4e9b-b1bc-7419d18c8ebb', N'Hanoi', NULL, N'employee 5', 2, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'2f990f30-fe1b-4d98-9d87-aaec98ea9766', N'nam22@gmail.com', N'1', N'111', 2, N'12345671')
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'5f2af64d-b1a5-42e7-aaef-dc0f2f2112dd', N'Hanoi', NULL, N'User 1', 3, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'7a53e1b8-003d-46b0-8c53-f0104d09ce23', N'Hanoi', NULL, N'User 2', 3, NULL)
/****** Object:  Table [dbo].[UserProducts]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProducts](
	[user_id] [uniqueidentifier] NOT NULL,
	[lst_product] [nvarchar](max) NULL,
 CONSTRAINT [PK_UserProducts] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Carts]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carts](
	[order_code] [uniqueidentifier] NOT NULL,
	[customer_address] [nvarchar](250) NULL,
	[customer_email] [nvarchar](250) NULL,
	[customer_name] [nvarchar](250) NULL,
	[customer_phone] [nvarchar](11) NULL,
	[note] [ntext] NULL,
	[order_date] [datetime2](7) NULL,
	[status_order] [int] NULL,
	[user_id] [uniqueidentifier] NULL,
 CONSTRAINT [PrimaryKey_CartId] PRIMARY KEY CLUSTERED 
(
	[order_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[product_id] [uniqueidentifier] NOT NULL,
	[category_id] [int] NULL,
	[code] [nvarchar](250) NULL,
	[date_created] [datetime2](7) NULL,
	[date_deleted] [datetime2](7) NULL,
	[date_updated] [datetime2](7) NULL,
	[expiry_date] [datetime2](7) NULL,
	[name] [nvarchar](250) NULL,
	[net_price] [decimal](18, 0) NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
	[provider_id] [int] NULL,
	[quantity] [int] NULL,
	[sale] [int] NULL,
	[status] [bit] NOT NULL,
	[user_id] [uniqueidentifier] NULL,
 CONSTRAINT [PrimaryKey_ProductId] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductDetails]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDetails](
	[product_id] [uniqueidentifier] NOT NULL,
	[description] [nvarchar](250) NULL,
	[img1] [nvarchar](250) NULL,
	[img2] [nvarchar](250) NULL,
	[img3] [nvarchar](250) NULL,
	[img4] [nvarchar](250) NULL,
	[img5] [nvarchar](250) NULL,
	[img6] [nvarchar](250) NULL,
	[keyWord] [nvarchar](250) NULL,
	[list_img] [image] NULL,
 CONSTRAINT [PrimaryKey_Product_Detail_Id] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderProducts]    Script Date: 11/06/2017 00:40:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProducts](
	[order_code] [uniqueidentifier] NOT NULL,
	[net_price] [decimal](18, 0) NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
	[product_code] [nvarchar](250) NULL,
	[product_id] [uniqueidentifier] NOT NULL,
	[quantity_number] [int] NOT NULL,
 CONSTRAINT [PrimaryKey_order_code] PRIMARY KEY CLUSTERED 
(
	[order_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [ForeignKey_Cart_User]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[Carts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Cart_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
GO
ALTER TABLE [dbo].[Carts] CHECK CONSTRAINT [ForeignKey_Cart_User]
GO
/****** Object:  ForeignKey [ForeignKey_Employee_User]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Employee_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [ForeignKey_Employee_User]
GO
/****** Object:  ForeignKey [ForeignKey_OrderProduct_Cart]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[OrderProducts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_OrderProduct_Cart] FOREIGN KEY([order_code])
REFERENCES [dbo].[Carts] ([order_code])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderProducts] CHECK CONSTRAINT [ForeignKey_OrderProduct_Cart]
GO
/****** Object:  ForeignKey [ForeignKey_OrderProduct_Product]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[OrderProducts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_OrderProduct_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Products] ([product_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderProducts] CHECK CONSTRAINT [ForeignKey_OrderProduct_Product]
GO
/****** Object:  ForeignKey [ForeignKey_ProductDetail_Product]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[ProductDetails]  WITH CHECK ADD  CONSTRAINT [ForeignKey_ProductDetail_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Products] ([product_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductDetails] CHECK CONSTRAINT [ForeignKey_ProductDetail_Product]
GO
/****** Object:  ForeignKey [ForeignKey_Product_Category]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Product_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Categories] ([category_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [ForeignKey_Product_Category]
GO
/****** Object:  ForeignKey [ForeignKey_Product_Provider]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Product_Provider] FOREIGN KEY([provider_id])
REFERENCES [dbo].[Providers] ([provider_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [ForeignKey_Product_Provider]
GO
/****** Object:  ForeignKey [ForeignKey_Product_User]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Product_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [ForeignKey_Product_User]
GO
/****** Object:  ForeignKey [ForeignKey_UserProduct_User]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[UserProducts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_UserProduct_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserProducts] CHECK CONSTRAINT [ForeignKey_UserProduct_User]
GO
/****** Object:  ForeignKey [ForeignKey_User_Role]    Script Date: 11/06/2017 00:40:56 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [ForeignKey_User_Role] FOREIGN KEY([role_id])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [ForeignKey_User_Role]
GO
