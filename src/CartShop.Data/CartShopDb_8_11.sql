USE [CartShopDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171102151423_InitialCreate', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171102173507_UpdateForeignKeyUserProduct', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171103033411_updateColumnNameForRoleEmp', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171105084254_AddOrderByTableMenu', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171105111156_AddIconTableMenu', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171106103638_UpdateStatusTbMenu', N'1.1.0-rtm-22752')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20171107171421_UpdateIdOrderProduct', N'1.1.0-rtm-22752')
/****** Object:  Table [dbo].[Menus]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[menu_id] [int] IDENTITY(1,1) NOT NULL,
	[level_user] [int] NULL,
	[menu_name] [nvarchar](250) NULL,
	[menu_url] [nvarchar](250) NULL,
	[parent_id] [int] NULL,
	[status] [bit] NOT NULL,
	[order_by] [int] NULL,
	[icon] [nvarchar](max) NULL,
 CONSTRAINT [PrimaryKey_MenuId] PRIMARY KEY CLUSTERED 
(
	[menu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Menus] ON
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (2, 1, N'Quản lý nhân viên', N'/Account', NULL, 1, 4, N'fa-users')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (3, 1, N'Quản lý danh mục ', N'/Category', NULL, 1, 3, N'fa-list')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (5, 1, N'Quản lý hàng hóa', N'/Products', NULL, 1, 2, N'fa-shopping-bag')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (6, 1, N'Quản lý bán hàng', N'/Carts', NULL, 1, 1, N'fa-cart-plus')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (7, 1, N'Báo cáo', N'#', NULL, 1, 5, N'fa-book')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (8, 2, N'Báo cáo hàng tồn kho', N'/Reports/tonkho', 7, 1, 1, N'fa-book')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (9, 2, N'Báo cáo doanh thu', N'/Reports/Index', 7, 1, 2, N'fa-book')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (10, 1, N'Config', N'#', NULL, 1, 6, N'fa-cogs')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (11, 1, N'Quản lý menu', N'/menu', 10, 1, 1, N'fa-list')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (12, 1, N'Settings', N'#', 10, 0, 2, N'fa-cog')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (13, 1, N'Quản lý nhà cung cấp', N'/provider', 10, 1, 3, N'fa-fire')
INSERT [dbo].[Menus] ([menu_id], [level_user], [menu_name], [menu_url], [parent_id], [status], [order_by], [icon]) VALUES (14, 1, N'xx', N'xxx', NULL, 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Menus] OFF
/****** Object:  Table [dbo].[Roles]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](250) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (N'fab4ccfd-6181-40bb-9314-0e45176d0845', N'User')
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', N'Employee')
INSERT [dbo].[Roles] ([Id], [RoleName]) VALUES (N'430c85c3-99ed-4b48-94f4-a78a7084fee9', N'Administrator')
/****** Object:  Table [dbo].[Providers]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Providers](
	[provider_id] [int] IDENTITY(1,1) NOT NULL,
	[provider_address] [nvarchar](250) NULL,
	[provider_name] [nvarchar](250) NULL,
	[provider_note] [nvarchar](250) NULL,
	[provider_rating] [int] NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PrimaryKey_ProviderId] PRIMARY KEY CLUSTERED 
(
	[provider_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Providers] ON
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (1, NULL, N'Sony', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (2, NULL, N'SamSung', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (3, NULL, N'Apple', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (4, NULL, N'Oppo', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (5, NULL, N'Adidas', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (6, NULL, N'Bitis', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (7, NULL, N'Vinamik', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (8, NULL, N'Made in china', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (9, NULL, N'Không rõ nguồn gốc', NULL, NULL, 1)
INSERT [dbo].[Providers] ([provider_id], [provider_address], [provider_name], [provider_note], [provider_rating], [status]) VALUES (10, NULL, N'Xách tay', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Providers] OFF
/****** Object:  Table [dbo].[Categories]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [nvarchar](250) NULL,
	[category_type] [int] NULL,
	[parent_cat_id] [int] NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PrimaryKey_CategoryId] PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (1, N'Điện tử & công nghệ', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (2, N'Gia dụng', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (3, N'Nội thất', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (4, N'Thời trang', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (5, N'Trang sức', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (6, N'Mẹ & bé', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (7, N'Sức khỏe & Làm đẹp', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (8, N'Du Lịch', NULL, NULL, 1)
INSERT [dbo].[Categories] ([category_id], [category_name], [category_type], [parent_cat_id], [status]) VALUES (9, N'Ô tô & xe máy', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Categories] OFF
/****** Object:  Table [dbo].[Users]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[user_id] [uniqueidentifier] NOT NULL,
	[change_pass] [int] NULL,
	[code_change_pass] [nvarchar](max) NULL,
	[date_created] [datetime2](7) NULL,
	[date_deleted] [datetime2](7) NULL,
	[date_updated] [datetime2](7) NULL,
	[email] [nvarchar](250) NULL,
	[email_confirm] [nvarchar](250) NULL,
	[login_status] [int] NULL,
	[pass_hash] [nvarchar](max) NULL,
	[pass_word] [nvarchar](250) NULL,
	[role_id] [uniqueidentifier] NOT NULL,
	[status] [bit] NOT NULL,
	[user_create] [uniqueidentifier] NULL,
	[user_name] [nvarchar](250) NULL,
 CONSTRAINT [PrimaryKey_UserId] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'14ecc699-cc6b-465c-87c0-462e9fdaf3f5', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste6@gmail.com', N'teste6@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee6')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'350ffd56-54db-4ce0-be51-466249106a73', NULL, NULL, CAST(0x07DF7051EC137D3D0B AS DateTime2), CAST(0x07A9109FA9177D3D0B AS DateTime2), NULL, N'sss@ff', N'sss@ff', 0, N'698d51a19d8a121ce581499d7b701668', N'111', N'fab4ccfd-6181-40bb-9314-0e45176d0845', 0, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'sss1')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'282b88bf-6523-46a5-a8d8-4b80e7e28e0b', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), CAST(0x07AE7F6A91177D3D0B AS DateTime2), CAST(0x072B2250B0127D3D0B AS DateTime2), N'teste4@gmail.com', N'teste4@gmail.com', NULL, N'c4ca4238a0b923820dcc509a6f75849b', N'1', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 0, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee4')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', NULL, NULL, CAST(0x073039F432067B3D0B AS DateTime2), NULL, CAST(0x077E3F9C7FC87D3D0B AS DateTime2), N'adminshop@cartshop.com', N'adminshop@cartshop.com', NULL, N'e10adc3949ba59abbe56e057f20f883e', N'123456', N'430c85c3-99ed-4b48-94f4-a78a7084fee9', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'admin')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'54d60694-b095-43e6-9390-6aa0cef88eb5', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste7@gmail.com', N'teste7@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee7')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'd7f9119b-1d6f-4b2f-90b8-6ec4cf1310d6', NULL, NULL, CAST(0x07D07D11C2067B3D0B AS DateTime2), NULL, NULL, N'teste2@gmail.com', N'teste2@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee3')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'860325c3-4b93-4e9b-b1bc-7419d18c8ebb', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste8@gmail.com', N'teste8@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee8')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'2f990f30-fe1b-4d98-9d87-aaec98ea9766', NULL, NULL, CAST(0x07A4E366E4C17C3D0B AS DateTime2), NULL, CAST(0x07D19C20870D7D3D0B AS DateTime2), N'nam22@gmail.com', N'nam22@gmail.com', 0, N'698d51a19d8a121ce581499d7b701668', N'111', N'fab4ccfd-6181-40bb-9314-0e45176d0845', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'nam22')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'5f2af64d-b1a5-42e7-aaef-dc0f2f2112dd', NULL, NULL, CAST(0x07D07D11C2067B3D0B AS DateTime2), NULL, NULL, N'teste1@gmail.com', N'teste1@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee1')
INSERT [dbo].[Users] ([user_id], [change_pass], [code_change_pass], [date_created], [date_deleted], [date_updated], [email], [email_confirm], [login_status], [pass_hash], [pass_word], [role_id], [status], [user_create], [user_name]) VALUES (N'7a53e1b8-003d-46b0-8c53-f0104d09ce23', NULL, NULL, CAST(0x07101A12C2067B3D0B AS DateTime2), NULL, NULL, N'teste5@gmail.com', N'teste5@gmail.com', NULL, NULL, N'Abc@123', N'f86d2a8b-f20a-4742-b1ae-36d4c37d92e6', 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'employee5')
/****** Object:  Table [dbo].[Employees]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[user_id] [uniqueidentifier] NOT NULL,
	[address] [nvarchar](250) NULL,
	[genner] [nvarchar](250) NULL,
	[employee_name] [nvarchar](250) NULL,
	[office] [int] NULL,
	[phone] [nvarchar](11) NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'14ecc699-cc6b-465c-87c0-462e9fdaf3f5', N'Hanoi', NULL, N'employee 1', 2, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'350ffd56-54db-4ce0-be51-466249106a73', N'111', N'1', N'111', 2, N'111')
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'282b88bf-6523-46a5-a8d8-4b80e7e28e0b', N'Hanoi', N'1', N'employee 2', 3, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d', N'Hanoi', N'1', N'Nguyễn Văn Nam', 3, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'd7f9119b-1d6f-4b2f-90b8-6ec4cf1310d6', N'Hanoi', NULL, N'employee 4', 2, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'860325c3-4b93-4e9b-b1bc-7419d18c8ebb', N'Hanoi', NULL, N'employee 5', 2, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'2f990f30-fe1b-4d98-9d87-aaec98ea9766', N'nam22@gmail.com', N'1', N'111', 2, N'12345671')
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'5f2af64d-b1a5-42e7-aaef-dc0f2f2112dd', N'Hanoi', NULL, N'User 1', 3, NULL)
INSERT [dbo].[Employees] ([user_id], [address], [genner], [employee_name], [office], [phone]) VALUES (N'7a53e1b8-003d-46b0-8c53-f0104d09ce23', N'Hanoi', NULL, N'User 2', 3, NULL)
/****** Object:  Table [dbo].[UserProducts]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProducts](
	[user_id] [uniqueidentifier] NOT NULL,
	[lst_product] [nvarchar](max) NULL,
 CONSTRAINT [PK_UserProducts] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Carts]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carts](
	[order_code] [uniqueidentifier] NOT NULL,
	[customer_address] [nvarchar](250) NULL,
	[customer_email] [nvarchar](250) NULL,
	[customer_name] [nvarchar](250) NULL,
	[customer_phone] [nvarchar](11) NULL,
	[note] [ntext] NULL,
	[order_date] [datetime2](7) NULL,
	[status_order] [int] NULL,
	[user_id] [uniqueidentifier] NULL,
 CONSTRAINT [PrimaryKey_CartId] PRIMARY KEY CLUSTERED 
(
	[order_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'09f5c04f-ac20-45af-8063-04f250cfbe37', N'nam', N'nam@gmail.com', N'nam', N'2312312', NULL, CAST(0x074AFDBCC5937F3D0B AS DateTime2), 3, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'dc89ec32-7872-4e4d-af58-20a1e1f0a99c', N'nam', N'nam@gmail.com', N'nam', N'100101', NULL, CAST(0x079F4544F8927F3D0B AS DateTime2), 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'39d8437d-9968-4625-82f6-2de338a4e35f', N'nam', N'nam@gmail.com', N'nam', N'2312312', NULL, CAST(0x0743A3DF26917F3D0B AS DateTime2), 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'6370d290-7258-4a1f-bd7a-3d66f996014b', N'nam', N'nam@gmail.com', N'nam', N'2312312', NULL, CAST(0x075A4676AC937F3D0B AS DateTime2), 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'fcd4c7b3-1f20-4df7-9288-5ee4863d4a55', N'aa', NULL, N'aa', N'aa', NULL, CAST(0x071D56A97704803D0B AS DateTime2), 3, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'83a12065-8792-45d4-b411-6f021f1cf5c0', N'nam', N'nam@gmail.com', N'nam', N'2312312', NULL, CAST(0x07D27C4E62937F3D0B AS DateTime2), 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'42021f1a-95ee-4a51-bc74-d518d43f1d50', N'nam', N'nam@gmail.com', N'nam', N'2312312', NULL, CAST(0x07FD136F30937F3D0B AS DateTime2), 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'f9ae05c5-f9b9-4509-88f3-f7ab468a3558', N'test', NULL, N'test', N'test', NULL, CAST(0x078341369E07803D0B AS DateTime2), 3, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Carts] ([order_code], [customer_address], [customer_email], [customer_name], [customer_phone], [note], [order_date], [status_order], [user_id]) VALUES (N'85bc1a5d-7257-486a-a6e0-ffc5c87a4fc8', N'Hà Nội', NULL, N'Nam', N'09991119922', NULL, CAST(0x070BC68F0B10803D0B AS DateTime2), 2, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
/****** Object:  Table [dbo].[Products]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[product_id] [uniqueidentifier] NOT NULL,
	[category_id] [int] NULL,
	[code] [nvarchar](250) NULL,
	[date_created] [datetime2](7) NULL,
	[date_deleted] [datetime2](7) NULL,
	[date_updated] [datetime2](7) NULL,
	[expiry_date] [datetime2](7) NULL,
	[name] [nvarchar](250) NULL,
	[net_price] [decimal](18, 0) NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
	[provider_id] [int] NULL,
	[quantity] [int] NULL,
	[sale] [int] NULL,
	[status] [bit] NOT NULL,
	[user_id] [uniqueidentifier] NULL,
 CONSTRAINT [PrimaryKey_ProductId] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Products] ([product_id], [category_id], [code], [date_created], [date_deleted], [date_updated], [expiry_date], [name], [net_price], [price], [provider_id], [quantity], [sale], [status], [user_id]) VALUES (N'bac38444-9cfc-4463-8df5-3ef5c10838b6', 1, NULL, CAST(0x074C51F070507F3D0B AS DateTime2), NULL, NULL, CAST(0x070000000000233F0B AS DateTime2), N'Samsung Galaxy Note 5', CAST(10900000 AS Decimal(18, 0)), CAST(12400000 AS Decimal(18, 0)), 2, 100, NULL, 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Products] ([product_id], [category_id], [code], [date_created], [date_deleted], [date_updated], [expiry_date], [name], [net_price], [price], [provider_id], [quantity], [sale], [status], [user_id]) VALUES (N'a95e22c1-5c55-4842-9a36-46e4563c99e4', 1, NULL, CAST(0x0785C8BEB3507F3D0B AS DateTime2), NULL, NULL, CAST(0x0700000000002E3F0B AS DateTime2), N'Samsung galaxy J5 Prime', CAST(4990000 AS Decimal(18, 0)), CAST(5100000 AS Decimal(18, 0)), 2, 100, NULL, 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Products] ([product_id], [category_id], [code], [date_created], [date_deleted], [date_updated], [expiry_date], [name], [net_price], [price], [provider_id], [quantity], [sale], [status], [user_id]) VALUES (N'd691fd8d-c3c5-4068-9b6e-7133b8bdc19c', 1, NULL, CAST(0x07A5DE94AA4F7F3D0B AS DateTime2), NULL, NULL, CAST(0x070000000000EF3E0B AS DateTime2), N'Điện thoại Samsung Galaxy S8 plus Xanh san hô - Chính hãng FPT', CAST(19900000 AS Decimal(18, 0)), CAST(20490000 AS Decimal(18, 0)), 2, 100, NULL, 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Products] ([product_id], [category_id], [code], [date_created], [date_deleted], [date_updated], [expiry_date], [name], [net_price], [price], [provider_id], [quantity], [sale], [status], [user_id]) VALUES (N'58ec9b21-16f0-46e5-8f2c-f5af1e9015ad', 1, NULL, CAST(0x075C22AE4F507F3D0B AS DateTime2), NULL, NULL, CAST(0x07000000000053400B AS DateTime2), N'Samsung Galaxy Note 8 - Đen', CAST(21900000 AS Decimal(18, 0)), CAST(22400000 AS Decimal(18, 0)), 2, 100, NULL, 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
INSERT [dbo].[Products] ([product_id], [category_id], [code], [date_created], [date_deleted], [date_updated], [expiry_date], [name], [net_price], [price], [provider_id], [quantity], [sale], [status], [user_id]) VALUES (N'455f6768-11d8-4c8b-a5c3-f9271ac98461', 1, NULL, CAST(0x0759D69093507F3D0B AS DateTime2), NULL, NULL, CAST(0x0700000000002D3F0B AS DateTime2), N'Samsung Galaxy J5 Prime', CAST(4440000 AS Decimal(18, 0)), CAST(4990000 AS Decimal(18, 0)), 2, 100, NULL, 1, N'0699cc8c-c58f-4e21-b9d5-5cdb1aee097d')
/****** Object:  Table [dbo].[ProductDetails]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDetails](
	[product_id] [uniqueidentifier] NOT NULL,
	[description] [nvarchar](250) NULL,
	[img1] [nvarchar](250) NULL,
	[img2] [nvarchar](250) NULL,
	[img3] [nvarchar](250) NULL,
	[img4] [nvarchar](250) NULL,
	[img5] [nvarchar](250) NULL,
	[img6] [nvarchar](250) NULL,
	[keyWord] [nvarchar](250) NULL,
	[list_img] [image] NULL,
 CONSTRAINT [PrimaryKey_Product_Detail_Id] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[ProductDetails] ([product_id], [description], [img1], [img2], [img3], [img4], [img5], [img6], [keyWord], [list_img]) VALUES (N'bac38444-9cfc-4463-8df5-3ef5c10838b6', NULL, N'https://i-shop.vnecdn.net/images/2016/11/17/582d6b19cbe98-635854272590343110_note5-g1.jpg', N'https://i-shop.vnecdn.net/images/2016/11/17/582d6b1cbacc3-635854272592059495_note5-g2.jpg', N'https://i-shop.vnecdn.net/images/2016/11/17/582d6b1f64926-635854272593775880_note5-g3.jpg', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ProductDetails] ([product_id], [description], [img1], [img2], [img3], [img4], [img5], [img6], [keyWord], [list_img]) VALUES (N'a95e22c1-5c55-4842-9a36-46e4563c99e4', NULL, N'https://i-shop.vnecdn.net/images/2017/01/20/588183ef09c54-thumbspe.jpg', N'https://i-shop.vnecdn.net/images/2017/03/22/58d2013059557-11.jpg', N'https://i-shop.vnecdn.net/images/2017/03/22/58d2013119c06-22.jpg', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ProductDetails] ([product_id], [description], [img1], [img2], [img3], [img4], [img5], [img6], [keyWord], [list_img]) VALUES (N'd691fd8d-c3c5-4068-9b6e-7133b8bdc19c', NULL, N'https://cf.shopee.vn/file/e42f187f510052c9e276a2bf6e33cee6_tn', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ProductDetails] ([product_id], [description], [img1], [img2], [img3], [img4], [img5], [img6], [keyWord], [list_img]) VALUES (N'58ec9b21-16f0-46e5-8f2c-f5af1e9015ad', NULL, N'https://i-shop.vnecdn.net/images/2017/09/20/59c1e0157f163-black.png', N'https://i-shop.vnecdn.net/images/2017/09/20/59c1ef604de64-d2.png', N'https://i-shop.vnecdn.net/images/2017/09/20/59c1ef61bfa06-d3.png', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ProductDetails] ([product_id], [description], [img1], [img2], [img3], [img4], [img5], [img6], [keyWord], [list_img]) VALUES (N'455f6768-11d8-4c8b-a5c3-f9271ac98461', N'Samsung Galaxy J5 Prime', N'https://i-shop.vnecdn.net/images/2017/01/04/586cae824cc62-636155073785868798_j5-prime-den-1.jpg', N'https://i-shop.vnecdn.net/images/2017/01/04/586cae82c0b88-636155073960748161_j5-prime-den-3.jpg', N'https://i-shop.vnecdn.net/images/2017/01/04/586cae833892e-636155073962152188_j5-prime-den-2.jpg', NULL, NULL, NULL, NULL, NULL)
/****** Object:  Table [dbo].[OrderProducts]    Script Date: 11/08/2017 02:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProducts](
	[order_code] [uniqueidentifier] NOT NULL,
	[net_price] [decimal](18, 0) NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
	[product_code] [nvarchar](250) NULL,
	[product_id] [uniqueidentifier] NOT NULL,
	[quantity_number] [int] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PrimaryKey_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[OrderProducts] ON
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'09f5c04f-ac20-45af-8063-04f250cfbe37', CAST(4990000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'a95e22c1-5c55-4842-9a36-46e4563c99e4', 1, 1)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'dc89ec32-7872-4e4d-af58-20a1e1f0a99c', CAST(4990000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'a95e22c1-5c55-4842-9a36-46e4563c99e4', 2, 2)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'39d8437d-9968-4625-82f6-2de338a4e35f', CAST(10900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'bac38444-9cfc-4463-8df5-3ef5c10838b6', 1, 3)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'6370d290-7258-4a1f-bd7a-3d66f996014b', CAST(4990000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'a95e22c1-5c55-4842-9a36-46e4563c99e4', 1, 4)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'83a12065-8792-45d4-b411-6f021f1cf5c0', CAST(4990000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'a95e22c1-5c55-4842-9a36-46e4563c99e4', 1, 5)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'42021f1a-95ee-4a51-bc74-d518d43f1d50', CAST(10900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'bac38444-9cfc-4463-8df5-3ef5c10838b6', 2, 6)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'f9ae05c5-f9b9-4509-88f3-f7ab468a3558', CAST(10900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'bac38444-9cfc-4463-8df5-3ef5c10838b6', 1, 7)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'f9ae05c5-f9b9-4509-88f3-f7ab468a3558', CAST(4990000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'a95e22c1-5c55-4842-9a36-46e4563c99e4', 2, 8)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'f9ae05c5-f9b9-4509-88f3-f7ab468a3558', CAST(19900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'd691fd8d-c3c5-4068-9b6e-7133b8bdc19c', 2, 9)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'fcd4c7b3-1f20-4df7-9288-5ee4863d4a55', CAST(10900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'bac38444-9cfc-4463-8df5-3ef5c10838b6', 2, 10)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'fcd4c7b3-1f20-4df7-9288-5ee4863d4a55', CAST(19900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'd691fd8d-c3c5-4068-9b6e-7133b8bdc19c', 1, 11)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'85bc1a5d-7257-486a-a6e0-ffc5c87a4fc8', CAST(4990000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'a95e22c1-5c55-4842-9a36-46e4563c99e4', 1, 12)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'85bc1a5d-7257-486a-a6e0-ffc5c87a4fc8', CAST(19900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'd691fd8d-c3c5-4068-9b6e-7133b8bdc19c', 1, 13)
INSERT [dbo].[OrderProducts] ([order_code], [net_price], [price], [product_code], [product_id], [quantity_number], [Id]) VALUES (N'85bc1a5d-7257-486a-a6e0-ffc5c87a4fc8', CAST(21900000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, N'58ec9b21-16f0-46e5-8f2c-f5af1e9015ad', 6, 14)
SET IDENTITY_INSERT [dbo].[OrderProducts] OFF
/****** Object:  ForeignKey [ForeignKey_Cart_User]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[Carts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Cart_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
GO
ALTER TABLE [dbo].[Carts] CHECK CONSTRAINT [ForeignKey_Cart_User]
GO
/****** Object:  ForeignKey [ForeignKey_Employee_User]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Employee_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [ForeignKey_Employee_User]
GO
/****** Object:  ForeignKey [ForeignKey_OrderProduct_Cart]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[OrderProducts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_OrderProduct_Cart] FOREIGN KEY([order_code])
REFERENCES [dbo].[Carts] ([order_code])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderProducts] CHECK CONSTRAINT [ForeignKey_OrderProduct_Cart]
GO
/****** Object:  ForeignKey [ForeignKey_OrderProduct_Product]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[OrderProducts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_OrderProduct_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Products] ([product_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderProducts] CHECK CONSTRAINT [ForeignKey_OrderProduct_Product]
GO
/****** Object:  ForeignKey [ForeignKey_ProductDetail_Product]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[ProductDetails]  WITH CHECK ADD  CONSTRAINT [ForeignKey_ProductDetail_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Products] ([product_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductDetails] CHECK CONSTRAINT [ForeignKey_ProductDetail_Product]
GO
/****** Object:  ForeignKey [ForeignKey_Product_Category]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Product_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Categories] ([category_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [ForeignKey_Product_Category]
GO
/****** Object:  ForeignKey [ForeignKey_Product_Provider]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Product_Provider] FOREIGN KEY([provider_id])
REFERENCES [dbo].[Providers] ([provider_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [ForeignKey_Product_Provider]
GO
/****** Object:  ForeignKey [ForeignKey_Product_User]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [ForeignKey_Product_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [ForeignKey_Product_User]
GO
/****** Object:  ForeignKey [ForeignKey_UserProduct_User]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[UserProducts]  WITH CHECK ADD  CONSTRAINT [ForeignKey_UserProduct_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([user_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserProducts] CHECK CONSTRAINT [ForeignKey_UserProduct_User]
GO
/****** Object:  ForeignKey [ForeignKey_User_Role]    Script Date: 11/08/2017 02:34:48 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [ForeignKey_User_Role] FOREIGN KEY([role_id])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [ForeignKey_User_Role]
GO
