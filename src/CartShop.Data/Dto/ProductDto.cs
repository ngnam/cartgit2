﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartShop.Data.Dto
{
    public class ProductDto
    {
        public Guid? product_id { get; set; }
        public string name { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public decimal net_price { get; set; }
        public DateTime? expiry_date { get; set; }
        public string code { get; set; }
        public int? provider_id { get; set; }
        public int? category_id { get; set; }
        public Guid? user_id { get; set; }
        public bool status { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public DateTime? date_deleted { get; set; }
        public string description { get; set; }
        public string keyWord { get; set; }
        public string img1 { get; set; }
        public string img2 { get; set; }
        public string img3 { get; set; }
        public string img4 { get; set; }
        public string img5 { get; set; }
        public string img6 { get; set; }
        public byte[] list_img { get; set; }
    }
}
