﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartShop.Data.Enum
{
    public enum ErrorCode
    {
        Invalid = 1,
        NotExist = 2,
        ErrorUpdate = 3,
        ErrorDelete = 4,
        ErrorAdd = 5,
        ErrorGet = 6,
        Error404 = 7,
        ErrorDeletePermision = 8,
    }

    public enum LoginStatus
    {
        login = 1,
        logout = 2,
    }

    public class Result<T>
    {
        public T Items { get; set; }

        public int Counts { get; set; }

        public int? limit { get; set; }

        public int? page { get; set; }

        public bool isSuccess { get; set; }

        public ErrorCode Error { get; set; }

        public class ErrorCode
        {
            public ErrorCode Code { get; set; }
            public string Message { get; set; }
        }
    }
}
