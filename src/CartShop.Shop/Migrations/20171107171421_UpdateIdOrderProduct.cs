﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CartShop.Shop.Migrations
{
    public partial class UpdateIdOrderProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PrimaryKey_order_code",
                table: "OrderProducts");

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "OrderProducts",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PrimaryKey_Id",
                table: "OrderProducts",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_order_code",
                table: "OrderProducts",
                column: "order_code");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PrimaryKey_Id",
                table: "OrderProducts");

            migrationBuilder.DropIndex(
                name: "IX_OrderProducts_order_code",
                table: "OrderProducts");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "OrderProducts");

            migrationBuilder.AddPrimaryKey(
                name: "PrimaryKey_order_code",
                table: "OrderProducts",
                column: "order_code");
        }
    }
}
