﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CartShop.Shop.Migrations
{
    public partial class UpdateForeignKeyUserProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "ForeignKey_UserProduct_User",
                table: "Users");

            migrationBuilder.AddForeignKey(
                name: "ForeignKey_UserProduct_User",
                table: "UserProducts",
                column: "user_id",
                principalTable: "Users",
                principalColumn: "user_id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "ForeignKey_UserProduct_User",
                table: "UserProducts");

            migrationBuilder.AddForeignKey(
                name: "ForeignKey_UserProduct_User",
                table: "Users",
                column: "user_id",
                principalTable: "UserProducts",
                principalColumn: "user_id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
