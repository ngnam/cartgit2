﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CartShop.Shop.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    category_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    category_name = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    category_type = table.Column<int>(type: "int", nullable: true),
                    parent_cat_id = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_CategoryId", x => x.category_id);
                });

            migrationBuilder.CreateTable(
                name: "Menus",
                columns: table => new
                {
                    menu_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    level_user = table.Column<int>(type: "int", nullable: true),
                    menu_name = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    menu_url = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    parent_id = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_MenuId", x => x.menu_id);
                });

            migrationBuilder.CreateTable(
                name: "Providers",
                columns: table => new
                {
                    provider_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    provider_address = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    provider_name = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    provider_note = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    provider_rating = table.Column<int>(nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_ProviderId", x => x.provider_id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserProducts",
                columns: table => new
                {
                    user_id = table.Column<Guid>(nullable: false),
                    lst_product = table.Column<string>(type: "nvarchar(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProducts", x => x.user_id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    user_id = table.Column<Guid>(nullable: false),
                    change_pass = table.Column<int>(nullable: true),
                    code_change_pass = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    date_created = table.Column<DateTime>(nullable: true),
                    date_deleted = table.Column<DateTime>(nullable: true),
                    date_updated = table.Column<DateTime>(nullable: true),
                    email = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    email_confirm = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    login_status = table.Column<int>(nullable: true),
                    pass_hash = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    pass_word = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    role_id = table.Column<Guid>(nullable: false),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    user_create = table.Column<Guid>(nullable: true),
                    user_name = table.Column<string>(type: "nvarchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_UserId", x => x.user_id);
                    table.ForeignKey(
                        name: "ForeignKey_User_Role",
                        column: x => x.role_id,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "ForeignKey_UserProduct_User",
                        column: x => x.user_id,
                        principalTable: "UserProducts",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Carts",
                columns: table => new
                {
                    order_code = table.Column<Guid>(nullable: false),
                    customer_address = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    customer_email = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    customer_name = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    customer_phone = table.Column<string>(type: "nvarchar(11)", nullable: true),
                    note = table.Column<string>(type: "ntext", maxLength: 1000, nullable: true),
                    order_date = table.Column<DateTime>(nullable: true),
                    status_order = table.Column<int>(type: "int", nullable: true),
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_CartId", x => x.order_code);
                    table.ForeignKey(
                        name: "ForeignKey_Cart_User",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    address = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    genner = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    name = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    office = table.Column<int>(type: "int", nullable: true),
                    phone = table.Column<string>(type: "nvarchar(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.user_id);
                    table.ForeignKey(
                        name: "ForeignKey_Employee_User",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    product_id = table.Column<Guid>(nullable: false),
                    category_id = table.Column<int>(nullable: true),
                    code = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    date_created = table.Column<DateTime>(nullable: true),
                    date_deleted = table.Column<DateTime>(nullable: true),
                    date_updated = table.Column<DateTime>(nullable: true),
                    expiry_date = table.Column<DateTime>(nullable: true),
                    name = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    net_price = table.Column<decimal>(type: "decimal(18, 0)", nullable: false),
                    price = table.Column<decimal>(type: "decimal(18, 0)", nullable: false),
                    provider_id = table.Column<int>(nullable: true),
                    quantity = table.Column<int>(type: "int", nullable: true),
                    sale = table.Column<int>(nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    user_id = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_ProductId", x => x.product_id);
                    table.ForeignKey(
                        name: "ForeignKey_Product_Category",
                        column: x => x.category_id,
                        principalTable: "Categories",
                        principalColumn: "category_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "ForeignKey_Product_Provider",
                        column: x => x.provider_id,
                        principalTable: "Providers",
                        principalColumn: "provider_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "ForeignKey_Product_User",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderProducts",
                columns: table => new
                {
                    order_code = table.Column<Guid>(nullable: false),
                    net_price = table.Column<float>(type: "decimal(18, 0)", nullable: false),
                    price = table.Column<float>(type: "decimal(18, 0)", nullable: false),
                    product_code = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    product_id = table.Column<Guid>(nullable: false),
                    quantity_number = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_order_code", x => x.order_code);
                    table.ForeignKey(
                        name: "ForeignKey_OrderProduct_Cart",
                        column: x => x.order_code,
                        principalTable: "Carts",
                        principalColumn: "order_code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "ForeignKey_OrderProduct_Product",
                        column: x => x.product_id,
                        principalTable: "Products",
                        principalColumn: "product_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductDetails",
                columns: table => new
                {
                    product_id = table.Column<Guid>(nullable: false),
                    description = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    img1 = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    img2 = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    img3 = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    img4 = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    img5 = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    img6 = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    keyWord = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    list_img = table.Column<byte[]>(type: "image", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_Product_Detail_Id", x => x.product_id);
                    table.ForeignKey(
                        name: "ForeignKey_ProductDetail_Product",
                        column: x => x.product_id,
                        principalTable: "Products",
                        principalColumn: "product_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Carts_user_id",
                table: "Carts",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_product_id",
                table: "OrderProducts",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_Products_category_id",
                table: "Products",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_Products_provider_id",
                table: "Products",
                column: "provider_id");

            migrationBuilder.CreateIndex(
                name: "IX_Products_user_id",
                table: "Products",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Users_role_id",
                table: "Users",
                column: "role_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Menus");

            migrationBuilder.DropTable(
                name: "OrderProducts");

            migrationBuilder.DropTable(
                name: "ProductDetails");

            migrationBuilder.DropTable(
                name: "Carts");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "Providers");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "UserProducts");
        }
    }
}
