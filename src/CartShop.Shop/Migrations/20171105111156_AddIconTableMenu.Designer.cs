﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CartShop.Models;

namespace CartShop.Shop.Migrations
{
    [DbContext(typeof(CartShopContext))]
    [Migration("20171105111156_AddIconTableMenu")]
    partial class AddIconTableMenu
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CartShop.Models.Cart", b =>
                {
                    b.Property<Guid>("order_code")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("customer_address")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("customer_email")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("customer_name")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("customer_phone")
                        .HasColumnType("nvarchar(11)");

                    b.Property<string>("note")
                        .HasColumnType("ntext")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("order_date");

                    b.Property<int?>("status_order")
                        .HasColumnType("int");

                    b.Property<Guid?>("user_id")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("order_code")
                        .HasName("PrimaryKey_CartId");

                    b.HasIndex("user_id");

                    b.ToTable("Carts");
                });

            modelBuilder.Entity("CartShop.Models.Category", b =>
                {
                    b.Property<int>("category_id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("category_name")
                        .HasColumnType("nvarchar(250)");

                    b.Property<int?>("category_type")
                        .HasColumnType("int");

                    b.Property<int?>("parent_cat_id")
                        .HasColumnType("int");

                    b.Property<bool>("status")
                        .HasColumnType("bit");

                    b.HasKey("category_id")
                        .HasName("PrimaryKey_CategoryId");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("CartShop.Models.Employee", b =>
                {
                    b.Property<Guid>("user_id")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("address")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("employee_name")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("genner")
                        .HasColumnType("nvarchar(250)");

                    b.Property<int?>("office")
                        .HasColumnType("int");

                    b.Property<string>("phone")
                        .HasColumnType("nvarchar(11)");

                    b.HasKey("user_id");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("CartShop.Models.Menu", b =>
                {
                    b.Property<int>("menu_id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("icon");

                    b.Property<int?>("level_user")
                        .HasColumnType("int");

                    b.Property<string>("menu_name")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("menu_url")
                        .HasColumnType("nvarchar(250)");

                    b.Property<int?>("order_by");

                    b.Property<int?>("parent_id")
                        .HasColumnType("int");

                    b.Property<bool?>("status")
                        .HasColumnType("bit");

                    b.HasKey("menu_id")
                        .HasName("PrimaryKey_MenuId");

                    b.ToTable("Menus");
                });

            modelBuilder.Entity("CartShop.Models.OrderProduct", b =>
                {
                    b.Property<Guid>("order_code");

                    b.Property<float>("net_price")
                        .HasColumnType("decimal(18, 0)");

                    b.Property<float>("price")
                        .HasColumnType("decimal(18, 0)");

                    b.Property<string>("product_code")
                        .HasColumnType("nvarchar(250)");

                    b.Property<Guid>("product_id");

                    b.Property<int>("quantity_number")
                        .HasColumnType("int");

                    b.HasKey("order_code")
                        .HasName("PrimaryKey_order_code");

                    b.HasIndex("product_id");

                    b.ToTable("OrderProducts");
                });

            modelBuilder.Entity("CartShop.Models.Product", b =>
                {
                    b.Property<Guid>("product_id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("category_id");

                    b.Property<string>("code")
                        .HasColumnType("nvarchar(250)");

                    b.Property<DateTime?>("date_created");

                    b.Property<DateTime?>("date_deleted");

                    b.Property<DateTime?>("date_updated");

                    b.Property<DateTime?>("expiry_date");

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(250)");

                    b.Property<decimal>("net_price")
                        .HasColumnType("decimal(18, 0)");

                    b.Property<decimal>("price")
                        .HasColumnType("decimal(18, 0)");

                    b.Property<int?>("provider_id");

                    b.Property<int?>("quantity")
                        .HasColumnType("int");

                    b.Property<int?>("sale");

                    b.Property<bool>("status")
                        .HasColumnType("bit");

                    b.Property<Guid?>("user_id");

                    b.HasKey("product_id")
                        .HasName("PrimaryKey_ProductId");

                    b.HasIndex("category_id");

                    b.HasIndex("provider_id");

                    b.HasIndex("user_id");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("CartShop.Models.ProductDetail", b =>
                {
                    b.Property<Guid>("product_id");

                    b.Property<string>("description")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("img1")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("img2")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("img3")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("img4")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("img5")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("img6")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("keyWord")
                        .HasColumnType("nvarchar(250)");

                    b.Property<byte[]>("list_img")
                        .HasColumnType("image");

                    b.HasKey("product_id")
                        .HasName("PrimaryKey_Product_Detail_Id");

                    b.ToTable("ProductDetails");
                });

            modelBuilder.Entity("CartShop.Models.Provider", b =>
                {
                    b.Property<int>("provider_id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("provider_address")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("provider_name")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("provider_note")
                        .HasColumnType("nvarchar(250)");

                    b.Property<int?>("provider_rating");

                    b.Property<bool>("status")
                        .HasColumnType("bit");

                    b.HasKey("provider_id")
                        .HasName("PrimaryKey_ProviderId");

                    b.ToTable("Providers");
                });

            modelBuilder.Entity("CartShop.Models.Role", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("RoleName")
                        .HasColumnType("nvarchar(250)");

                    b.HasKey("Id");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("CartShop.Models.User", b =>
                {
                    b.Property<Guid>("user_id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("change_pass");

                    b.Property<string>("code_change_pass")
                        .HasColumnType("nvarchar(MAX)");

                    b.Property<DateTime?>("date_created");

                    b.Property<DateTime?>("date_deleted");

                    b.Property<DateTime?>("date_updated");

                    b.Property<string>("email")
                        .HasColumnType("nvarchar(250)");

                    b.Property<string>("email_confirm")
                        .HasColumnType("nvarchar(250)");

                    b.Property<int?>("login_status");

                    b.Property<string>("pass_hash")
                        .HasColumnType("nvarchar(MAX)");

                    b.Property<string>("pass_word")
                        .HasColumnType("nvarchar(250)");

                    b.Property<Guid>("role_id");

                    b.Property<bool>("status")
                        .HasColumnType("bit");

                    b.Property<Guid?>("user_create");

                    b.Property<string>("user_name")
                        .HasColumnType("nvarchar(250)");

                    b.HasKey("user_id")
                        .HasName("PrimaryKey_UserId");

                    b.HasIndex("role_id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("CartShop.Models.UserProduct", b =>
                {
                    b.Property<Guid>("user_id");

                    b.Property<string>("lst_product")
                        .HasColumnType("nvarchar(MAX)");

                    b.HasKey("user_id");

                    b.ToTable("UserProducts");
                });

            modelBuilder.Entity("CartShop.Models.Cart", b =>
                {
                    b.HasOne("CartShop.Models.User", "User")
                        .WithMany("Cart")
                        .HasForeignKey("user_id")
                        .HasConstraintName("ForeignKey_Cart_User");
                });

            modelBuilder.Entity("CartShop.Models.Employee", b =>
                {
                    b.HasOne("CartShop.Models.User", "User")
                        .WithOne("Employee")
                        .HasForeignKey("CartShop.Models.Employee", "user_id")
                        .HasConstraintName("ForeignKey_Employee_User")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CartShop.Models.OrderProduct", b =>
                {
                    b.HasOne("CartShop.Models.Cart", "Cart")
                        .WithMany("OrderProduct")
                        .HasForeignKey("order_code")
                        .HasConstraintName("ForeignKey_OrderProduct_Cart")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CartShop.Models.Product", "Product")
                        .WithMany("OrderProduct")
                        .HasForeignKey("product_id")
                        .HasConstraintName("ForeignKey_OrderProduct_Product")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CartShop.Models.Product", b =>
                {
                    b.HasOne("CartShop.Models.Category", "Category")
                        .WithMany("Product")
                        .HasForeignKey("category_id")
                        .HasConstraintName("ForeignKey_Product_Category");

                    b.HasOne("CartShop.Models.Provider", "Provider")
                        .WithMany("Product")
                        .HasForeignKey("provider_id")
                        .HasConstraintName("ForeignKey_Product_Provider");

                    b.HasOne("CartShop.Models.User", "User")
                        .WithMany("Product")
                        .HasForeignKey("user_id")
                        .HasConstraintName("ForeignKey_Product_User");
                });

            modelBuilder.Entity("CartShop.Models.ProductDetail", b =>
                {
                    b.HasOne("CartShop.Models.Product", "Product")
                        .WithOne("ProductDetail")
                        .HasForeignKey("CartShop.Models.ProductDetail", "product_id")
                        .HasConstraintName("ForeignKey_ProductDetail_Product")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CartShop.Models.User", b =>
                {
                    b.HasOne("CartShop.Models.Role", "Role")
                        .WithMany("User")
                        .HasForeignKey("role_id")
                        .HasConstraintName("ForeignKey_User_Role")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CartShop.Models.UserProduct", b =>
                {
                    b.HasOne("CartShop.Models.User", "User")
                        .WithOne("UserProduct")
                        .HasForeignKey("CartShop.Models.UserProduct", "user_id")
                        .HasConstraintName("ForeignKey_UserProduct_User")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
