﻿using System.ComponentModel.DataAnnotations;

namespace CartShop.ViewModel
{
    public class CategoryViewModel
    {
        public int? category_id { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập tên danh mục hàng hóa")]
        [Display(Name = "Tên danh mục")]
        [MaxLength(250, ErrorMessage = "Không vượt quá 250 kí tự")]
        public string category_name { get; set; }
        public int? parent_cat_id { get; set; }
        public int? category_type { get; set; }
        [Display(Name = "Trạng thái")]
        public bool status { get; set; }
    }
}
