﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CartShop.ViewModel
{
    public class CartViewModel
    {
        [Display(Name = "Mã đơn hàng")]
        public Guid? order_code { get; set; }
        [Display(Name = "Ngày đặt hàng")]
        public DateTime? order_date { get; set; }
        [Display(Name = "Tên khách hàng")]
        [Required(ErrorMessage = "Vui lòng nhập Họ và tên")]
        [MaxLength(250, ErrorMessage = "Tên khách hàng không vượt quá 250 kí tự")]
        public string customer_name { get; set; }
        [Display(Name = "Số điện thoại liên hệ")]
        //[Required(ErrorMessage = "Vui lòng nhập số điện thoại")]
        [MaxLength(11, ErrorMessage = "Số điện thoại không vượt quá 11 kí tự")]
        public string customer_phone { get; set; }
        [Display(Name = "Địa chỉ liên hệ")]
        [Required(ErrorMessage = "Vui lòng nhập địa chỉ liên hệ")]
        public string customer_address { get; set; }
        [Display(Name = "Địa chỉ email")]
        [EmailAddress(ErrorMessage = "Địa chỉ e-mail chưa đúng định dạng.")]
        public string customer_email { get; set; }
        [Display(Name = "Ghi chú đơn hàng")]
        public string note { get; set; }
        [Display(Name = "Trạng thái đơn hàng")]
        public int? status_order { get; set; }
        public Guid? user_id { get; set; }
        public string TenNguoiTao { get; set; }
        public string manhanvien { get; set; }
        public IList<OrderProductViewModel> OrderProduct { get; set; } = new List<OrderProductViewModel>() { };
    }

    public class OrderProductViewModel
    {
        [Display(Name = "Mã hàng")]
        public Guid product_id { get; set; }
        [Display(Name = "Tên sản phẩm")]
        public string product_name { get; set; }
        [Display(Name = "Mã code")]
        public string product_code { get; set; }
        [Display(Name = "Giá bán")]
        public decimal net_price
        {
            get; set;
        }
        [Display(Name = "Giá nhập")]
        public decimal price
        {
            get; set;
        }
        [Display(Name = "Số lượng")]
        [Required(ErrorMessage = "Vui lòng nhập số lượng")]
        public int? quantity_number { get; set; }
        public CartViewModel Cart { get; set; }
        public ProductForOrderViewModel Product { get; set; }
        [Display(Name = "Nhà sản xuất")]
        public string provinder_name { get; set; }
        [Display(Name = "Đơn vị")]
        public string product_unit
        {
            get
            {
                return "ĐVT";
            }
            private set { }
        }
        public decimal? TongBan
        {
            get
            {
                return net_price * quantity_number;
            }

            private set { }
        }
        public decimal? TongNhạp {
            get
            {
                return price * quantity_number;
            }

            private set { }
        }
    }

    public class ProductForOrderViewModel
    {
        [Display(Name = "Mã hàng")]
        public Guid? product_id { get; set; }
        [Display(Name = "Tên sản phẩm")]
        public string product_name { get; set; }
        [Display(Name = "Giá bán")]
        public decimal net_price { get; set; }
        [Display(Name = "Giá nhập")]
        public decimal price { get; set; }
        [Display(Name = "Nhà sản xuất")]
        public string provinder_name { get; set; }
        [Display(Name = "Đơn vị")]
        public string product_unit
        {
            get
            {
                return "ĐVT";
            }
            private set { }
        }
    }
}
