﻿using CartShop.Data.Enum;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CartShop.ViewModel
{
    public class UserManagerViewModel
    {
        public Guid user_id { get; set; }
        public string user_name { get; set; }
        public string email { get; set; }
        public string email_confirm { get; set; }
        public Guid? user_create { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public DateTime? date_deleted { get; set; }
        public int? login_status { get; set; }
        public string status_text
        {
            get
            {
                var text = "Offline";
                if (login_status.HasValue)
                {
                    text = login_status.Value == (int)LoginStatus.login ? "Online" : "Offline";
                }
                return text;
            }
            private set { }
        }
        public bool status { get; set; }
        public string name { get; set; }
        public string genner { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public int? office { get; set; }
        public string RoleName { get; set; }
    }

    public class UserViewModel
    {
        public Guid user_id { get; set; }
        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "Vui lòng nhập tên đăng nhập")]
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        public string user_name { get; set; }
        [Required(ErrorMessage ="Vui lòng nhập {0}")]
        [EmailAddress(ErrorMessage = "Địa chỉ e-mail chưa đúng định dạng.")]
        [Display(Name = "Email")]
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        public string email { get; set; }
        [Compare("email", ErrorMessage = "{0} chưa khớp")]
        [Display(Name = "Email xác nhận")]
        [MaxLength(250, ErrorMessage = "{0} không vượt quá 250 kí tự")]
        public string email_confirm { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [Display(Name = "Mật khẩu")]
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        public string pass_word { get; set; }
        [Compare("pass_word", ErrorMessage = "{0} chưa khớp")]
        [Display(Name = "Mật khẩu xác nhận")]
        public string pass_confirm { get; set; }
        public string pass_hash { get; set; }
        public string code_change_pass { get; set; }
        public int? change_pass { get; set; }
        public Guid? user_create { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public DateTime? date_deleted { get; set; }
        public int? login_status { get; set; }
        [Display(Name = "Kích hoạt")]
        public bool status { get; set; }
        [Display(Name = "Quyền hạng")]
        public Guid? role_id { get; set; }
        public bool? IsEdit { get; set; }

        public EmployeeViewModel Employee { get; set; } = new EmployeeViewModel();
    }

    public class EmployeeViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập {0}")]
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        [Display(Name = "Họ và tên")]
        public string name { get; set; }
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        [Display(Name = "Giới tính")]
        public string genner { get; set; }
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        [Display(Name = "Địa chỉ")]
        public string address { get; set; }
        [MaxLength(11, ErrorMessage = "{0} Không vượt quá 11 kí tự")]
        [Display(Name = "Số điện thoại")]
        public string phone { get; set; }
        [Display(Name = "Chức vụ")]
        public int? office { get; set; }
    }

    public class UserDeleteViewModel
    {
        public Guid user_id { get; set; }
        public string user_name { get; set; }
        public string email { get; set; }
    }

    public class RoleViewModel
    {
        public Guid role_id { get; set; }
        public string RoleName { get; set; }
    }

    public class CurrentUser
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập {0}")]
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        [Display(Name = "Tên đăng nhập hoặc email")]
        public string user_name_or_email { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập {0}")]
        [MaxLength(250, ErrorMessage = "{0} Không vượt quá 250 kí tự")]
        [Display(Name = "Mật khẩu")]
        public string pass_word { get; set; }
        [Display(Name = "Ghi nhớ mật khẩu")]
        public bool isRemember { get; set; }
    }

    public class ChangePassViewModel
    {
        [Display(Name = "Mật khẩu cũ")]
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu cũ")]
        public string pass_word_old { get; set; }
        [Display(Name = "Mật khẩu mới")]
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu mới")]
        public string pass_word_new { get; set; }
        [Display(Name = "Mật khẩu xác nhận")]
        [Required(ErrorMessage = "Vui lòng nhập xác nhận mật khẩu")]
        [Compare("pass_word_new", ErrorMessage = "Mật khẩu xác nhận chưa khớp")]
        public string pass_word_confirm { get; set; }
    }
}
