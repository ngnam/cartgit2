﻿using System.ComponentModel.DataAnnotations;

namespace CartShop.ViewModel
{
    public class ProviderViewModel
    {
        public int? provider_id { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập tên nhà cung cấp")]
        [Display(Name = "Tên nhà cung cấp")]
        [MaxLength(250, ErrorMessage = "Tên không vượt quá 250 kí tự")]
        public string provider_name { get; set; }
        [Display(Name = "Địa chỉ")]
        public string provider_address { get; set; }
        [Display(Name = "Đánh giá")]
        public int? provider_rating { get; set; }
        [Display(Name = "Ghi chú")]
        public string provider_note { get; set; }
        [Display(Name = "Trạng thái")]
        public bool status { get; set; }
    }
}
