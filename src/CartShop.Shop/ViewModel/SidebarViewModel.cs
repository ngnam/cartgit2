﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CartShop.ViewModel
{
    public class SidebarViewModel
    {
        public int? menu_id { get; set; }
        public string menu_name { get; set; }
        public string menu_url { get; set; }
        public int? order_by { get; set; }
        public int? parent_id { get; set; }
        public string icon { get; set; }
        public IList<SidebarViewModel> childMenu { get; set; } = new List<SidebarViewModel>() { };
    }

    public class MenuViewModel
    {        
        public int? menu_id { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập tên menu")]
        [Display(Name = "Tên menu")]
        [MaxLength(250, ErrorMessage = "Menu không vượt quá 250 kí tự")]
        public string menu_name { get; set; }
        [Display(Name = "Đường dẫn truy cập")]
        [Required(ErrorMessage = "Vui lòng nhập đường dẫn menu")]
        public string menu_url { get; set; }
        [Display(Name = "Trạng thái")]
        public bool status { get; set; }
        [Display(Name = "Cấp độ truy cập")]
        public int? level_user { get; set; }
        [Display(Name = "ID Danh mục cha")]
        public int? parent_id { get; set; }
        [Display(Name = "Vị trí sắp xếp")]
        public int? order_by { get; set; }
        [Display(Name = "Icon hiện thị")]
        public string icon { get; set; }
    }

}
