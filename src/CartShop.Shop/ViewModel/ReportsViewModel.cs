﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartShop.ViewModel
{
    public class ReportsViewModel
    {
        public DateTime? OrderDate { get; set; }
        public int TotalOrder { get; set; }
        public decimal? TotalMoney { get; set; }
    }

    public class TonKhoViewModel
    {
        public DateTime? OrderDate { get; set; }
        public Guid? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? SL_Nhap { get; set; }
        public int? SL_Xuat { get; set; }
        public int? SL_TonKho {
            get
            {
                if (SL_Nhap != null && SL_Xuat != null)
                {
                    return SL_Nhap - SL_Xuat;
                }
                return 0;
            }
            private set { }
        }
    }
}
