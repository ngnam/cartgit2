﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CartShop.ViewModel
{
    public class ProductViewModel
    {
        [Display(Name = "Mã hàng hóa")]
        public Guid? product_id { get; set; }
        [Display(Name = "Tên hàng hóa")]
        [Required(ErrorMessage = "Vui lòng nhập tên hàng hóa")]
        [MaxLength(250, ErrorMessage = "Tên hàng hóa Không vượt quá 250 kí tự")]
        public string name { get; set; }
        [Display(Name = "Số lượng hàng hóa")]
        [Required(ErrorMessage = "Vui lòng nhập số lượng hàng hóa")]
        public int? quantity { get; set; } = 0;
        [Display(Name = "Giá nhập")]
        [Required(ErrorMessage = "Vui lòng nhập giá nhập hàng hóa")]
        public decimal price { get; set; }
        [Display(Name = "Giá bán")]
        [Required(ErrorMessage = "Vui lòng nhập giá bán hàng hóa")]
        public decimal net_price { get; set; }
        [Display(Name = "Phần trăm giảm giá")]
        public int? sale
        {
            get; set;
        }
        [Display(Name = "Hạn sử dụng")]
        [Required(ErrorMessage = "Vui lòng nhập hạn sử dụng hàng hóa")]
        public DateTime? expiry_date { get; set; }
        public string code { get; set; }
        public Guid? user_id { get; set; }
        [Display(Name = "Trạng thái")]
        [Required(ErrorMessage = "Trạng thái hàng hóa phải chọn")]
        public bool status { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public DateTime? date_deleted { get; set; }
        [Display(Name = "Danh mục hàng hóa")]
        [Required(ErrorMessage = "Vui lòng chọn danh mục hàng hóa")]
        public int? category_id { get; set; }
        public string category_name { get; set; }
        [Display(Name = "Nhà cung cấp")]
        [Required(ErrorMessage = "Vui lòng chọn danh mục nhà cung cấp")]
        public int? provider_id { get; set; }        
        public string provider_name { get; set; }
        public virtual ProductDetailViewModel ProductDetail { get; set; }
    }

    public class ProductDetailViewModel
    {
        [Display(Name = "Mô tả (SEO)")]
        public string description { get; set; }
        [Display(Name = "Từ khóa Keyword (SEO)")]
        public string keyWord { get; set; }
        public string img1 { get; set; }
        public string img2 { get; set; }
        public string img3 { get; set; }
        public string img4 { get; set; }
        public string img5 { get; set; }
        public string img6 { get; set; }
    }

    public class DropDownCat
    {
        public int? category_id { get; set; }
        public string category_name { get; set; }
    }

    public class DropDownProvider
    {
        public int? provider_id { get; set; }
        public string provider_name { get; set; }
    }


    public class productSearch
    {
        public Guid product_id { get; set; }
        public string product_name { get; set; }
        public decimal price { get; set; }
        public decimal net_price { get; set; }
        public string dvt { get; set; }
        public string provider_name { get; set; }
    }

}
