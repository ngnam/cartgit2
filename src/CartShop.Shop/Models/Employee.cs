﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class Employee
    {
        [Column(TypeName = "uniqueidentifier")]
        public Guid user_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string employee_name { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string genner { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string address { get; set; }
        [Column(TypeName = "nvarchar(11)")]
        public string phone { get; set; }
        [Column(TypeName = "int")]
        public int? office { get; set; }

        public virtual User User { get; set; }
    }

    public enum LevelUser
    {
        employee = 1, // quản lý 
        admin = 2, // Nhân viên
        user = 3, // người dùng thường
    }
}
