﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class User
    {
        public User()
        {
            Product = new Collection<Product>();
            Cart = new Collection<Cart>();
            Employee = new Employee();
            UserProduct = new UserProduct();
        }
        
        public Guid user_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string user_name { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string email { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string email_confirm { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string pass_word { get; set; }
        [Column(TypeName = "nvarchar(MAX)")]
        public string pass_hash { get; set; }
        [Column(TypeName = "nvarchar(MAX)")]
        public string code_change_pass { get; set; }
        public int? change_pass { get; set; }
        public Guid? user_create { get; set; }        
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public DateTime? date_deleted { get; set; }
        public int? login_status { get; set; }
        [Column(TypeName = "bit")]
        public bool status { get; set; }
        public Guid role_id { get; set; }
        public Role Role { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual UserProduct UserProduct { get; set; }
        public ICollection<Product> Product { get; set; }
        public ICollection<Cart> Cart { get; set; }
    }
   
}
