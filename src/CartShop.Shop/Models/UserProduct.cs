﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class UserProduct
    {
        public Guid user_id { get; set; }
        [Column(TypeName = "nvarchar(MAX)")]
        public string lst_product { get; set; } 
        public virtual User User { get; set; }
    }
}
