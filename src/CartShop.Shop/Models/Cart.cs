﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class Cart
    {
        public Cart()
        {
            OrderProduct = new Collection<OrderProduct>();
        }

        public Guid order_code { get; set; } 
        public DateTime? order_date { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string customer_name { get; set; }
        [Column(TypeName = "nvarchar(11)")]
        public string customer_phone { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string customer_address { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string customer_email { get; set; }
        [Column(TypeName = "ntext"), MaxLength(1000)]
        public string note { get; set; }
        [Column(TypeName = "int")]
        public int? status_order { get; set; }
        [Column(TypeName = "uniqueidentifier")]
        public Guid? user_id { get; set; }
        public User User { get; set; }
        public virtual ICollection<OrderProduct> OrderProduct { get; set; }

    }


    public enum StatusOrder
    {
        IsNewOrder = 1,
        isDelivery = 2,
        IsPayed = 3,
        IsCanceled = 4
    }
    // status_code: 1-đơn hàng mới, 2-đang chuyển giao,3-đã thanh toán, 4-đã hủy
}
