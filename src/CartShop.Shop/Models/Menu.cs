﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class Menu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int menu_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string menu_name { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string menu_url { get; set; }
        [Column(TypeName = "bit")]
        public bool status { get; set; }
        [Column(TypeName = "int")]
        public int? level_user { get; set; }
        [Column(TypeName = "int")]
        public int? parent_id { get; set; }
        public int? order_by { get; set; }
        public string icon { get; set; }
    }
}
