﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace CartShop.Models
{
    public class CartShopContext : DbContext
    {
        public CartShopContext(DbContextOptions<CartShopContext> options) : base(options)
        {

        }

        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<UserProduct> UserProducts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<OrderProduct> OrderProducts { get; set; }
        public virtual DbSet<ProductDetail> ProductDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Role>()
                .ToTable("Roles")
                .HasKey(x => x.Id);

            modelBuilder.Entity<User>()
                .ToTable("Users")
                .HasKey(b => b.user_id)
                .HasName("PrimaryKey_UserId");

            // one to many
            modelBuilder.Entity<User>()
                .HasOne(p => p.Role)
                .WithMany(b => b.User)
                .HasForeignKey(p => p.role_id)
                .HasConstraintName("ForeignKey_User_Role");

            modelBuilder.Entity<Provider>()
                .ToTable("Providers")
                .HasKey(b => b.provider_id)
                .HasName("PrimaryKey_ProviderId");

            modelBuilder.Entity<Category>()
                .ToTable("Categories")
                .HasKey(b => b.category_id)
                .HasName("PrimaryKey_CategoryId");
            
            modelBuilder.Entity<Product>()
                .ToTable("Products")
                .HasKey(b => b.product_id)
                .HasName("PrimaryKey_ProductId");

            modelBuilder.Entity<Product>()
                .HasOne(p => p.User)
                .WithMany(b => b.Product)
                .HasForeignKey(p=>p.user_id)
                .HasConstraintName("ForeignKey_Product_User");

            modelBuilder.Entity<Product>()
                .HasOne(p => p.Provider)
                .WithMany(b => b.Product)
                .HasForeignKey(p => p.provider_id)
                .HasConstraintName("ForeignKey_Product_Provider");

            // one to many
            modelBuilder.Entity<Product>()
                .HasOne(p => p.Category)
                .WithMany(b => b.Product)
                .HasForeignKey(p => p.category_id)
                .HasConstraintName("ForeignKey_Product_Category");

            modelBuilder.Entity<Menu>()
                .ToTable("Menus")
                .HasKey(b => b.menu_id)
                .HasName("PrimaryKey_MenuId");
                    

            modelBuilder.Entity<UserProduct>()
                .ToTable("UserProducts")
                .HasKey(b => b.user_id);

            // one to one
            modelBuilder.Entity<UserProduct>()
                .HasOne(a => a.User)
                .WithOne(b => b.UserProduct)
                .HasForeignKey<UserProduct>(b => b.user_id)
                .HasConstraintName("ForeignKey_UserProduct_User");

            modelBuilder.Entity<Employee>()
                .ToTable("Employees")
                .HasKey(b => b.user_id);

            modelBuilder.Entity<Employee>()
                .HasOne(a => a.User)
                .WithOne(b => b.Employee)
                .HasForeignKey<Employee>(b => b.user_id)
                .HasConstraintName("ForeignKey_Employee_User");

            modelBuilder.Entity<Cart>()
                .ToTable("Carts")
                .HasKey(b => b.order_code)
                .HasName("PrimaryKey_CartId");

            modelBuilder.Entity<Cart>()
                .HasOne(p => p.User)
                .WithMany(b => b.Cart)
                .HasForeignKey(p => p.user_id)
                .HasConstraintName("ForeignKey_Cart_User");

            modelBuilder.Entity<OrderProduct>()
                .ToTable("OrderProducts")
                .HasKey(b => b.Id)
                .HasName("PrimaryKey_Id");

            modelBuilder.Entity<OrderProduct>()
                .HasOne(p => p.Cart)
                .WithMany(b => b.OrderProduct)
                .HasForeignKey(p => p.order_code)
                .HasConstraintName("ForeignKey_OrderProduct_Cart");

            //modelBuilder.Entity<Post>().HasRequired(n => n.Blog)
            //.WithMany(n => n.Posts)
            //.HasForeignKey(n => n.BlogId)
            //.WillCascadeOnDelete(true);

            modelBuilder.Entity<OrderProduct>()
                .HasOne(p => p.Product)
                .WithMany(b => b.OrderProduct)
                .HasForeignKey(p => p.product_id)
                .HasConstraintName("ForeignKey_OrderProduct_Product");

            modelBuilder.Entity<ProductDetail>()
                .ToTable("ProductDetails")
                .HasKey(b => b.product_id)
                .HasName("PrimaryKey_Product_Detail_Id");

            modelBuilder.Entity<ProductDetail>()
                .HasOne(p => p.Product)
                .WithOne(b => b.ProductDetail)
                .HasForeignKey<ProductDetail>(p => p.product_id)
                .HasConstraintName("ForeignKey_ProductDetail_Product");                                            

                        
        }
    }
}
