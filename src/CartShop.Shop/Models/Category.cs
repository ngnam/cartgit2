﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class Category
    {
        public Category()
        {

        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        public int category_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string category_name { get; set; }
        [Column(TypeName = "int")]
        public int? parent_cat_id { get; set; }
        [Column(TypeName = "int")]
        public int? category_type { get; set; }
        [Column(TypeName = "bit")]
        public bool status { get; set; }
        
        public ICollection<Product> Product { get; set; }
    }
}
