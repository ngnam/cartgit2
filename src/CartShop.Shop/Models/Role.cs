﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class Role
    {
        public Role()
        {

        }
   
        public Guid Id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string RoleName { get; set; }

        public ICollection<User> User { get; set; } 
    }
}
