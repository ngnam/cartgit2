﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class Product
    {
        public Product()
        {
            OrderProduct = new Collection<OrderProduct>();
        }

        public Guid product_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string name { get; set; }
        [Column(TypeName = "int")]
        public int? quantity { get; set; } = 0;
        [Column(TypeName = "decimal(18, 0)")]
        public decimal price { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal net_price { get; set; }
        public int? sale
        {
            get; set;
        }
        public DateTime? expiry_date { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string code { get; set; }        
        public Guid? user_id { get; set; }
        public User User { get; set; }
        [Column(TypeName = "bit")]
        public bool status { get; set; }
        public DateTime? date_created { get; set; }
        public DateTime? date_updated { get; set; }
        public DateTime? date_deleted { get; set; }
        public int? category_id { get; set; }
        public Category Category { get; set; }
        public int? provider_id { get; set; }
        public Provider Provider { get; set; }
        public virtual ProductDetail ProductDetail { get; set; }
        public ICollection<OrderProduct> OrderProduct { get; set; }
    }
}
