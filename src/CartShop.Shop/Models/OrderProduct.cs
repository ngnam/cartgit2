﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class OrderProduct
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public Guid order_code { get; set; }
        public Guid product_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string product_code { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal net_price { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal price { get; set; }
        [Column(TypeName = "int")]
        public int quantity_number { get; set; }

        public Cart Cart { get; set; }
        public Product Product { get; set; }
    }
}
