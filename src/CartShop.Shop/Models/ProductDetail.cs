﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class ProductDetail
    {
        public Guid product_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string description { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string keyWord { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string img1 { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string img2 { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string img3 { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string img4 { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string img5 { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string img6 { get; set; }
        [Column(TypeName = "image")]
        public byte[] list_img { get; set; }

        public virtual Product Product { get; set; }
    }
}
