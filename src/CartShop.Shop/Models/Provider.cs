﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartShop.Models
{
    public class Provider
    {
        public Provider()
        {

        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int provider_id { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string provider_name { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string provider_address { get; set; }
        public int? provider_rating { get; set; }
        [Column(TypeName = "nvarchar(250)")]
        public string provider_note { get; set; }
        [Column(TypeName = "bit")]
        public bool status { get; set; }
        public ICollection<Product> Product { get; set; }
    }
}
