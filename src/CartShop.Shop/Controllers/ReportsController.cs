﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CartShop.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using CartShop.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CartShop.Shop.Controllers
{
    [Authorize(Roles = "Administrator, Employee")]
    public class ReportsController : Base.BaseController<ReportsController>
    {
        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public ReportsController(
             ILoggerFactory loggerFactory,
             IOptions<CartShopConfiguration> cartShopConfiguration,
             CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<ReportsController>();
            _context = context;
        }

        // GET: Báo cáo doanh số
        public async Task<IActionResult> Index(string currentFilter, string searchString, int? page, string fromDate, string toDate)
        {
            ViewData["FromDate"] = fromDate;
            ViewData["ToDate"] = toDate;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var datas = _context.Carts
                                .Include(a => a.OrderProduct)
                                .Where(a => a.status_order == (int)StatusOrder.IsPayed);

            // check data null
            if (datas == null || datas.Count() == 0)
            {
                return RedirectToAction("Create", "Carts");
            }

            if (fromDate != null)
            {
                var Date1 = DateTime.Parse(fromDate);
                datas = datas.Where(a => a.order_date >= Date1);
            }

            if (toDate != null)
            {
                var Date2 = DateTime.Parse(toDate);
                datas = datas.Where(a => a.order_date <= Date2);
            }
                       

            if (!String.IsNullOrEmpty(searchString))
            {
                // support search with name, email, phone
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                datas = datas.Where(s => s.customer_email.Contains(searchString) || s.customer_name.Contains(searchString) || s.customer_phone.Contains(searchString));
            }

            datas = datas.OrderByDescending(s => s.order_date);



            // Group by order_date
            //var reports = datas.GroupBy(d => new {  d.order_date, d.OrderProduct })
            //                       .Select(g => new ReportsViewModel
            //                       {
            //                           OrderDate = g.Key.order_date,
            //                           TotalMoney = g.Key.OrderProduct.Sum(o => o.net_price * o.quantity_number), 
            //                           TotalOrder = g.Key.OrderProduct.Count
            //                       });

            var reports = datas.Select(g => new ReportsViewModel
            {
                OrderDate = g.order_date,
                TotalOrder = g.OrderProduct.Count,
                TotalMoney = g.OrderProduct.Sum(o => o.net_price * o.quantity_number),
               
            });
            //net_price là giá bán, price là giá nhập

            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<ReportsViewModel>.CreateAsync(reports.AsNoTracking(), page ?? 1, _pageSize));
        }


        // GET: Báo cáo tồn kho
        public async Task<IActionResult> tonkho(string currentFilter, string searchString, int? page, string fromDate, string toDate)
        {
            ViewData["FromDate"] = fromDate;
            ViewData["ToDate"] = toDate;
            //if (toDate == new DateTime()) toDate = DateTime.Now;
            //ViewData["FromDate"] = fromDate;
            //ViewData["ToDate"] = toDate;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var datas = _context.OrderProducts
                                .Include(a => a.Product)
                                .Include(a => a.Cart)
                                .Where(a => a.Cart.status_order == (int)StatusOrder.IsPayed);

            // check data null
            if (datas == null || datas.Count() == 0)
            {
                return RedirectToAction("Create", "Carts");
            }

            if (fromDate != null)
            {
                var Date1 = DateTime.Parse(fromDate);
                datas = datas.Where(a => a.Cart.order_date >= Date1);
            }

            if (toDate != null)
            {
                var Date2 = DateTime.Parse(toDate);
                datas = datas.Where(a => a.Cart.order_date <= Date2);
            }


            if (!String.IsNullOrEmpty(searchString))
            {
                // support search with name, email, phone
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                datas = datas.Where(s => s.Product.name.Contains(searchString));
            }

            datas = datas.OrderByDescending(s => s.Cart.order_date);



            // Group by 
            //var reports = datas.GroupBy(d => new { d.product_id, d.Product, d.Cart.order_date })
            //                   .Select(g => new 
            //                   {
            //                       OrderDate = g.Key.order_date,
            //                       ProductId = g.Key.product_id,
            //                       Product = g.Key.Product
            //                   })
            //                   .Select(a=>new TonKhoViewModel {
            //                        OrderDate = a.OrderDate,
            //                        ProductId = a.Product.product_id,
            //                        ProductName = a.Product.name,
            //                        SL_Nhap = a.Product.quantity,
            //                        SL_Xuat = a.Product.OrderProduct.Sum(op=>op.quantity_number)
            //                   });
            //var results = persons.GroupBy(n => new { n.PersonId, n.car })
            //    .Select(g => new {
            //        g.Key.PersonId,
            //        g.Key.car)}).ToList();

            var reports = datas.Select(g => new TonKhoViewModel {
                OrderDate = g.Cart.order_date,
                ProductId =g.product_id,
                ProductName = g.Product.name,
                SL_Nhap = g.Product.quantity,
                SL_Xuat = g.quantity_number,
            });

            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<TonKhoViewModel>.CreateAsync(reports.AsNoTracking(), page ?? 1, _pageSize));
        }


        protected override void Dispose(bool disposing)
        {
            if (_context == null)
            {
                return;
            }
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
