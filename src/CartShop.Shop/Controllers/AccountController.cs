﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CartShop.Models;
using CartShop.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using CartShop.Helpers.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http.Authentication;
using System.Collections.Generic;

namespace CartShop.Shop.Controllers
{

    public class AccountController : Base.BaseController<AccountController>
    {
        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public AccountController(
            ILoggerFactory loggerFactory,
            IOptions<CartShopConfiguration> cartShopConfiguration,
            CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _context = context;
        }

        // GET: /<controller>/
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["EmailSortParm"] = sortOrder == "email" ? "email_desc" : "email";
            ViewData["UserNameSortParm"] = sortOrder == "username" ? "username_desc" : "username";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var users = from u in _context.Users
                        where !u.date_deleted.HasValue
                        select new UserManagerViewModel()
                        {
                            user_id = u.user_id,
                            user_name = u.user_name,
                            email = u.email,
                            email_confirm = u.email_confirm,
                            user_create = u.user_create,
                            date_created = u.date_created,
                            date_updated = u.date_updated,
                            date_deleted = u.date_deleted,
                            login_status = u.login_status,
                            status = u.status,
                            RoleName = u.Role.RoleName,
                            name = u.Employee.employee_name,
                            address = u.Employee.address,
                            genner = u.Employee.genner,
                            office = u.Employee.office,
                            phone = u.Employee.phone
                        };

            // check data null
            if (users == null || users.Count() == 0)
            {
                return RedirectToAction("CreateUser");
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                users = users.Where(s => s.user_name.Contains(searchString)
                                       || s.email.Contains(searchString)
                                       || s.name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(s => s.name);
                    break;
                case "email":
                    users = users.OrderBy(s => s.email);
                    break;
                case "email_desc":
                    users = users.OrderByDescending(s => s.email);
                    break;
                case "username":
                    users = users.OrderBy(s => s.user_name);
                    break;
                case "username_desc":
                    users = users.OrderByDescending(s => s.user_name);
                    break;
                default:
                    users = users.OrderBy(s => s.name);
                    break;
            }
            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<UserManagerViewModel>.CreateAsync(users.AsNoTracking(), page ?? 1, _pageSize));
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult CreateUser()
        {
            ViewData["NewUserId"] = new Guid();
            // Load List RoleUser
            ViewData["RoleList"] = LoadListRoleUser();
            return View();
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // create new usser entity object
                    var user = new User();
                    // Assign student name
                    user.user_id = Guid.NewGuid();
                    user.user_name = model.user_name ?? null;
                    user.email = model.email ?? null;
                    user.email_confirm = model.email_confirm ?? null;
                    user.pass_word = model.pass_word ?? "Abc@123";
                    user.pass_hash = user.pass_word.GenPassWord();
                    user.user_create = AccountId;
                    user.role_id = model.role_id ?? new Guid();
                    user.date_created = DateTime.Now;
                    user.login_status = 0;
                    user.status = model.status;

                    // Create new Employee entity and assign it to user entity
                    user.Employee = new Employee()
                    {
                        user_id = user.user_id,
                        address = model.Employee.address ?? null,
                        employee_name = model.Employee.name ?? null,
                        genner = model.Employee.genner ?? null,
                        office = model.Employee.office ?? null,
                        phone = model.Employee.phone ?? null
                    };

                    //Add user object into Users's EntitySet
                    _context.Users.Add(user);
                    // call SaveChanges method to save user & Employee into database
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "Thêm mới nhân viên thành công.";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add user {ex.ToString()}");
                    TempData["Errors"] = "Có lỗi xảy ra khi thêm mới nhân viên.";
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        private List<SelectListItem> LoadListRoleUser()
        {
            // Load List RoleUser
            var rolelist = _context.Roles.ToList();
            return rolelist.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.RoleName
            }).ToList();
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> EditUser(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var user = await _context.Users.Include(x=>x.Employee).FirstOrDefaultAsync(x=>x.user_id.Equals(Id));
            if (user == null)
            {
                TempData["Errors"] = "Người dùng không tồn tại.";
                return RedirectToAction("Index");
            }
            // Load List RoleUser
            ViewData["RoleList"] = LoadListRoleUser();
            var model = new UserViewModel()
            {
                user_id = user.user_id,
                user_name = user.user_name,
                email = user.email,
                email_confirm = user.email_confirm,
                status = user.status,
                role_id = user.role_id,
                pass_word = user.pass_word,
                pass_confirm =user.pass_word,
                IsEdit = true
            };
            model.Employee = new EmployeeViewModel()
            {
                name = user.Employee.employee_name,
                address = user.Employee.address,
                genner = user.Employee.genner,
                office = user.Employee.office,
                phone = user.Employee.phone
            };            
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DetailUser(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var user = await _context.Users.Include(x => x.Employee).FirstOrDefaultAsync(x => x.user_id.Equals(Id) && x.status == true);
            if (user == null)
            {
                TempData["Errors"] = "Người dùng không tồn tại hoặc đã bị xóa.";
                return RedirectToAction("Index");
            }
            // Load List RoleUser
            ViewData["RoleList"] = LoadListRoleUser();
            var model = new UserViewModel()
            {
                user_id = user.user_id,
                user_name = user.user_name ,
                email = user.email,
                email_confirm = user.email_confirm,
                status = user.status,
                role_id = user.role_id,
                pass_word = user.pass_word,
                pass_confirm = user.pass_word,
                IsEdit = true
            };
            model.Employee = new EmployeeViewModel()
            {
                name = user.Employee.employee_name ?? null,
                address = user.Employee.address ?? null,
                genner = user.Employee.genner ?? null,
                office = user.Employee.office ?? null,
                phone = user.Employee.phone ?? null
            };
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUser(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.user_id == new Guid())
                    {
                        return View("Index");
                    }

                    var user = await _context.Users.Include(a=>a.Employee).FirstOrDefaultAsync(x=>x.user_id.Equals(model.user_id));
                    if (user == null)
                    {
                        TempData["Errors"] = "Người dùng không tồn tại trong hệ thống.";
                        return View("Index");
                    }

                    if (user.Employee != null)
                        _context.Entry<Employee>(user.Employee).State = EntityState.Deleted;
                    _context.Employees.Remove(user.Employee);
                    await _context.SaveChangesAsync();

                    // update new
                    user.user_name = model.user_name ?? null;
                    user.email = model.email ?? null;
                    user.email_confirm = model.email_confirm ?? null;
                    user.pass_word = model.pass_word ?? "Abc@123";
                    user.pass_hash = user.pass_word.GenPassWord();
                    user.user_create = AccountId;
                    user.role_id = model.role_id ?? new Guid();
                    user.date_updated = DateTime.Now;
                    user.status = model.status;                                     

                    // Create new Employee entity and assign it to user entity
                    user.Employee = new Employee()
                    {
                        user_id = user.user_id,
                        address = model.Employee.address ?? null,
                        employee_name = model.Employee.name ?? null,
                        genner = model.Employee.genner ?? null,
                        office = model.Employee.office ?? null,
                        phone = model.Employee.phone ?? null
                    };
                                       
                    //Mark Student entity as modified
                    _context.Entry(user).State = EntityState.Modified;                                        
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "Cập nhật thông tin thành công.";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add user {ex.ToString()}");
                    // Load List RoleUser
                    ViewData["RoleList"] = LoadListRoleUser();
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi cập nhật nhân viên.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteUser(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var user = await _context.Users.FirstOrDefaultAsync(x => x.user_id.Equals(Id) && x.status == true);
            if (user == null)
            {
                TempData["Errors"] = "Người dùng không tồn tại hoặc đã bị xóa.";
                return RedirectToAction("Index");
            }
            var model = new UserDeleteViewModel()
            {
                email = user.email,
                user_id = user.user_id,
                user_name = user.user_name
            };
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var user = await _context.Users.FirstOrDefaultAsync(x => x.user_id.Equals(Id) && x.status == true);
            if (user == null)
            {
                TempData["Errors"] = "Người dùng không tồn tại hoặc đã bị xóa.";
                return RedirectToAction("Index");
            }
            user.date_deleted = DateTime.Now;
            user.status = false;
            _context.Entry<User>(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            TempData["Success"] = "Xóa người dùng thành công.";
            return RedirectToAction("Index");
        }


        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Login(string ReturnUrl = null)
        {
            if (ReturnUrl == null) ReturnUrl = "/";
            if (User.Identity.IsAuthenticated)
            {
                return Redirect(ReturnUrl);
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("user_name_or_email, pass_word, isRemember")] LoginViewModel model, string ReturnUrl = null)
        {
            ViewData["ReturnUrl"] = ReturnUrl;
            if (ModelState.IsValid)
            {
                try
                {
                    string passHash = model.pass_word.GenPassWord();
                    // check user exist
                    var user = await _context.Users.Include(a=>a.Role).FirstOrDefaultAsync(x => (x.user_name.Equals(model.user_name_or_email) || x.email.Equals(model.user_name_or_email)) && x.pass_hash.Equals(passHash) && x.status == true);
                    if (user == null)
                    {
                        ModelState.AddModelError(string.Empty, "Sai tên đăng nhập hoặc mật khẩu.");
                        return View(model);
                    }
                    // save current user
                    var claimIdentity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                    claimIdentity.AddClaim(new Claim(ClaimTypes.Name, user.user_name));
                    claimIdentity.AddClaim(new Claim(ClaimTypes.UserData, user.email));
                    claimIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.user_id.ToString()));

                    // find Role
                    if (user.Role != null)
                    {
                        claimIdentity.AddClaim(new Claim(ClaimTypes.Role, user.Role.RoleName)); //Administrator
                    }

                    // save userstore Role
                    var claim = new ClaimsPrincipal(claimIdentity);

                    await HttpContext.Authentication.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claim,
                               new AuthenticationProperties
                               {
                                   IssuedUtc = System.DateTime.UtcNow,
                                   ExpiresUtc = System.DateTime.UtcNow.AddSeconds(_cartShopConfiguration.IdleTimeout)
                               });
                    // Return to admin page   
                    if (string.IsNullOrEmpty(ReturnUrl))
                    {
                        ReturnUrl = "/Products/Index";
                    }
                    if(user.Role.RoleName == "Employee")
                        ReturnUrl = "/Carts";

                    return Redirect(ReturnUrl);

                }
                catch (Exception ex)
                {
                    _logger.LogError($"-error login {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Đã xảy ra lỗi khi đăng nhập.");
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }

        [Authorize]
        public IActionResult ChangePass()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> ChangePass(ChangePassViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Check pass_word_old
                    var passWordHash = model.pass_word_old.GenPassWord();
                    var UserChangePass = _context.Users.FirstOrDefault(x => x.user_id == AccountId && x.pass_hash == passWordHash);
                    if (UserChangePass == null)
                    {
                        ModelState.AddModelError(string.Empty, "Mật khẩu cũ chưa đúng.");
                        return View(model);
                    }

                    var newPass = model.pass_word_new.GenPassWord();
                    UserChangePass.pass_word = model.pass_word_new;
                    UserChangePass.pass_hash = newPass;
                    UserChangePass.date_updated = DateTime.Now;
                    _context.Entry<User>(UserChangePass).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    // Login Again
                    await HttpContext.Authentication.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                    return RedirectToAction("Login", "Account");

                }
                catch (Exception ex) 
                {
                    _logger.LogError($"Error change pass {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy khi đổi mật khẩu.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (_context == null)
            {
                return;
            }
            _context.Dispose();
            base.Dispose(disposing);
        }

    }
}
