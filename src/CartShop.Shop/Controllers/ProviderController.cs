﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CartShop.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using CartShop.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using CartShop.Helpers.Extensions;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CartShop.Shop.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ProviderController : Base.BaseController<ProviderController>
    {
        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public ProviderController(
             ILoggerFactory loggerFactory,
             IOptions<CartShopConfiguration> cartShopConfiguration,
             CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<ProviderController>();
            _context = context;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["ProviderNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "provider_name_desc" : "";
            ViewData["ProviderAddressSortParm"] = String.IsNullOrEmpty(sortOrder) ? "provider_address_desc" : "";
            ViewData["ProviderRatingSortParm"] = String.IsNullOrEmpty(sortOrder) ? "provider_rating_desc" : "";
            ViewData["ProviderNoteSortParm"] = String.IsNullOrEmpty(sortOrder) ? "provider_note_desc" : "";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var datas = from u in _context.Providers
                        select new ProviderViewModel
                        {
                           provider_id = u.provider_id,
                           provider_address = u.provider_address,
                           provider_name = u.provider_name,
                           provider_note = u.provider_note,
                           provider_rating = u.provider_rating,
                           status = u.status                           
                        };

            // check data null
            if (datas == null)
            {
                return RedirectToAction("Create");
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                datas = datas.Where(s => s.provider_name.Contains(searchString) || s.provider_note.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "provider_name_desc":
                    datas = datas.OrderByDescending(s => s.provider_name);
                    break;
                case "provider_address_desc":
                    datas = datas.OrderByDescending(s => s.provider_address);
                    break;
                case "provider_rating_desc":
                    datas = datas.OrderByDescending(s => s.provider_rating);
                    break;
                case "provider_note_desc":
                    datas = datas.OrderByDescending(s => s.provider_note);
                    break;
                default:
                    datas = datas.OrderBy(s => s.provider_name);
                    break;
            }
            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<ProviderViewModel>.CreateAsync(datas.AsNoTracking(), page ?? 1, _pageSize));
        }


        public IActionResult Create()
        {
            ViewData["fakeMaNhaCc"] = Util.GenerateRandomString(6);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProviderViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // create new provider entity object
                    var provider = new Provider();
                    provider.provider_address = model.provider_address ?? null;
                    provider.provider_name = model.provider_name ?? null;
                    provider.provider_note = model.provider_note ?? null;
                    provider.provider_rating = model.provider_rating ?? null;
                    provider.status = model.status;

                    //Add provider object into Users's EntitySet
                    _context.Providers.Add(provider);
                    // call SaveChanges method to save provider into database
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "Thêm mới nhà sản xuất/xuất xứ sản phẩm thành công.";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add provider {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi thêm mới provider.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        public async Task<IActionResult> Edit(int? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var provider = await _context.Providers.FirstOrDefaultAsync(x => x.provider_id.Equals(Id));
            if (provider == null)
            {
                TempData["Errors"] = "Nhà sản xuất không tồn tại.";
                return RedirectToAction("Index");
            }
           
            var model = new ProviderViewModel()
            {
              provider_id = provider.provider_id,
              provider_address = provider.provider_address,
              provider_name = provider.provider_name,
              provider_note = provider.provider_note,
              provider_rating = provider.provider_rating,
              status = provider.status
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProviderViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!model.provider_id.HasValue)
                    {
                        return View("Index");
                    }

                    var provider = await _context.Providers.FirstOrDefaultAsync(x => x.provider_id.Equals(model.provider_id));
                    if (provider == null)
                    {
                        ViewData["Errors"] = "Danh mục nhà sản xuất không tồn tại trong hệ thống.";
                        return View("Index");
                    }

                    provider.provider_address = model.provider_address ?? null;
                    provider.provider_name = model.provider_name ?? null;
                    provider.provider_note = model.provider_note ?? null;
                    provider.provider_rating = model.provider_rating ?? null;
                    provider.status = model.status;

                    //Mark provider entity as modified
                    _context.Entry(provider).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "Cập nhật thông tin thành công.";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add user {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi cập nhật.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? Id)
        {
            if (!Id.HasValue)
            {
                return BadRequest();
            }
            var provider = await _context.Providers.Include(x=>x.Product).FirstOrDefaultAsync(x => x.provider_id.Equals(Id) && x.status == true);
            if (provider == null)
            {
                return BadRequest();
            }

            if (provider.Product.Count > 0)
            {
                return Json(0);
            }

            _context.Providers.Remove(provider);
            _context.Entry<Provider>(provider).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

            return Json(1);
        }

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int? Id)
        //{
        //    if (!Id.HasValue)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    var provider = await _context.Providers.FirstOrDefaultAsync(x => x.provider_id.Equals(Id) && x.status);
        //    if (provider == null)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    provider.status = false;
        //    _context.Entry<Provider>(provider).State = EntityState.Modified;
        //    await _context.SaveChangesAsync();
        //    TempData["Success"] = "Xóa danh mục nhà sản xuất thành công.";
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
