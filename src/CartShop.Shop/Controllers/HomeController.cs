﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using CartShop.ViewModel;
using CartShop.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CartShop.Shop.Controllers
{
    [Authorize]
    public class HomeController : Base.BaseController<HomeController>
    {       

        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public HomeController(
            ILoggerFactory loggerFactory,
            IOptions<CartShopConfiguration> cartShopConfiguration,
            CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _context = context;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Index", "Products");
        }

        public IActionResult Error()
        {
            return View();
        }
               

       
    }
}
