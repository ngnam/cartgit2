﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CartShop.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using CartShop.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CartShop.Shop.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CategoryController : Base.BaseController<CategoryController>
    {
        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public CategoryController(
             ILoggerFactory loggerFactory,
             IOptions<CartShopConfiguration> cartShopConfiguration,
             CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<CategoryController>();
            _context = context;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CatNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "cat_name_desc" : "";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var datas = from u in _context.Categories
                        select new CategoryViewModel
                        {
                           category_id = u.category_id,
                           category_name = u.category_name,
                           category_type = u.category_type,
                           parent_cat_id = u.parent_cat_id,
                           status = u.status
                        };

            // check data null
            if (datas == null || datas.Count() == 0)
            {
                return RedirectToAction("Create");
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                datas = datas.Where(s => s.category_name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "cat_name_desc":
                    datas = datas.OrderByDescending(s => s.category_name);
                    break;
                default:
                    datas = datas.OrderBy(s => s.category_name);
                    break;
            }
            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<CategoryViewModel>.CreateAsync(datas.AsNoTracking(), page ?? 1, _pageSize));
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // create new cat entity object
                    var cat = new Category();
                    cat.category_name = model.category_name ?? null;
                    cat.status = model.status;
                    //Add cat object into Users's EntitySet
                    _context.Categories.Add(cat);
                    // call SaveChanges method to save user & Employee into database
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "Thêm mới danh mục thành công.";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add user {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi thêm mới danh mục.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        public async Task<IActionResult> Edit(int? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var cat = await _context.Categories.FirstOrDefaultAsync(x => x.category_id.Equals(Id));
            if (cat == null)
            {
                TempData["Errors"] = "Danh mục không tồn tại.";
                return RedirectToAction("Index");
            }
           
            var model = new CategoryViewModel()
            {
               category_id = cat.category_id,
               category_name = cat.category_name,
               status = cat.status
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!model.category_id.HasValue)
                    {
                        return RedirectToAction("Index");
                    }

                    var cat = await _context.Categories.FirstOrDefaultAsync(x => x.category_id.Equals(model.category_id));
                    if (cat == null)
                    {
                        TempData["Errors"] = "Danh mục không tồn tại trong hệ thống.";
                        return RedirectToAction("Index");
                    }

                    cat.category_name = model.category_name ?? null;
                    cat.status = model.status;
                                       
                    //Mark cat entity as modified
                    _context.Entry(cat).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "Cập nhật thông tin thành công.";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add user {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi cập nhật.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        public async Task<IActionResult> Delete(int? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var cat = await _context.Categories.FirstOrDefaultAsync(x => x.category_id.Equals(Id) && x.status == true);
            if (cat == null)
            {
                return RedirectToAction("Index");
            }
            var model = new CategoryViewModel()
            {
                category_id = cat.category_id,
                category_name = cat.category_name
            };
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var cat = await _context.Categories.FirstOrDefaultAsync(x => x.category_id.Equals(Id) && x.status == true);
            if (cat == null)
            {
                return RedirectToAction("Index");
            }
            cat.status = false;
            _context.Entry<Category>(cat).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            // update products for cat = 0;
            TempData["Success"] = "Xóa danh mục thành công.";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
