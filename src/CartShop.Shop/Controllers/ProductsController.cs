﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CartShop.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using CartShop.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CartShop.Shop.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ProductsController : Base.BaseController<ProductsController>
    {
        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public ProductsController(
             ILoggerFactory loggerFactory,
             IOptions<CartShopConfiguration> cartShopConfiguration,
             CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<ProductsController>();
            _context = context;
        }

        private List<SelectListItem> LoadListCategories()
        {
            // Load List datas
            var datas = _context.Categories.Where(x=>x.status == true).ToList();
            var _lstSelect = datas.Select(x => new SelectListItem
            {
                Value = x.category_id.ToString(),
                Text = x.category_name
            }).ToList();
            _lstSelect.Insert(0, new SelectListItem() { Value = "", Text = "--Chọn danh sách--" });
            return _lstSelect;
        }

        private List<SelectListItem> LoadListProviders()
        {
            // Load List datas
            var datas = _context.Providers.Where(x => x.status == true).ToList();
            var _lstSelect = datas.Select(x => new SelectListItem
            {
                Value = x.provider_id.ToString(),
                Text = x.provider_name
            }).ToList();
            _lstSelect.Insert(0, new SelectListItem() { Value = "", Text = "--Chọn danh sách--" });
            return _lstSelect;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page, int? catId)
        {
            ViewData["ProductIdSortParm"] = String.IsNullOrEmpty(sortOrder) ? "product_id_desc" : "";
            ViewData["ProductNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "product_name_desc" : "";
            ViewData["ProductPriceSortParm"] = String.IsNullOrEmpty(sortOrder) ? "product_price_desc" : "";
            ViewData["ProductNetPriceSortParm"] = String.IsNullOrEmpty(sortOrder) ? "product_netprice_desc" : "";
            ViewData["ProviderNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "provider_name_desc" : "";
            ViewData["ProductQuantitySortParm"] = String.IsNullOrEmpty(sortOrder) ? "product_quantity_desc" : "";
            ViewData["ProductExpiryDateSortParm"] = String.IsNullOrEmpty(sortOrder) ? "product_expiry_date_desc" : "";
            ViewData["CurrentSort"] = sortOrder;
            ViewData["CatId"] = catId;
            ViewData["DropdownCat"] = LoadListCategories();
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var datas = _context.Products
                                .Include(p => p.Category)
                                .Include(p => p.Provider)
                                .Select(p => new ProductViewModel {
                                    product_id = p.product_id,
                                    name = p.name,
                                    quantity = p.quantity,
                                    price = p.price,
                                    net_price = p.net_price,
                                    expiry_date = p.expiry_date,
                                    code = p.code,
                                    user_id = p.user_id,
                                    status = p.status,
                                    date_created = p.date_created,
                                    date_deleted = p.date_deleted,
                                    date_updated = p.date_updated,
                                    category_id = p.category_id,
                                    category_name = p.Category.category_name,
                                    provider_id = p.provider_id,
                                    provider_name = p.Provider.provider_name
                                });
            // check data null
            if (datas == null || datas.Count() == 0)
            {
                return RedirectToAction("Create");
            }

            if (catId != null)
            {
                int _catID = 0;
                if (int.TryParse(catId.Value.ToString(), out _catID))
                {
                    datas = datas.Where(x => x.category_id == catId);
                }               
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                datas = datas.Where(s => s.provider_name.Contains(searchString) || 
                                    s.name.Contains(searchString) ||
                                    s.category_name.Contains(searchString));
            }
                       

            switch (sortOrder)
            {
                case "product_id_desc":
                    datas = datas.OrderByDescending(s => s.product_id);
                    break;
                case "product_name_desc":
                    datas = datas.OrderByDescending(s => s.name);
                    break;
                case "product_price_desc":
                    datas = datas.OrderByDescending(s => s.price);
                    break;
                case "product_netprice_desc":
                    datas = datas.OrderByDescending(s => s.net_price);
                    break;
                case "provider_name_desc":
                    datas = datas.OrderByDescending(s => s.provider_name);
                    break;
                case "product_quantity_desc":
                    datas = datas.OrderByDescending(s => s.quantity);
                    break;
                case "product_expiry_date_desc":
                    datas = datas.OrderByDescending(s => s.expiry_date);
                    break;
                default:
                    datas = datas.OrderBy(s => s.name);
                    break;
            }
            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<ProductViewModel>.CreateAsync(datas.AsNoTracking(), page ?? 1, _pageSize));
        }


        public IActionResult Create()
        {
            ViewData["NewProductId"] = new Guid();
            ViewData["DropdownCat"] = LoadListCategories();
            ViewData["DropdownProvider"] = LoadListProviders();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // create new product entity object
                    var product = new Product();
                    // Assign product name
                    product.product_id = Guid.NewGuid();
                    product.name = model.name ?? null;
                    product.quantity = model.quantity ?? null;
                    product.price = model.price;
                    product.net_price = model.net_price;
                    product.expiry_date = model.expiry_date ?? null;
                    product.user_id = AccountId;
                    product.status = model.status;
                    product.date_created = DateTime.Now;
                    product.category_id = model.category_id ?? null;
                    product.provider_id = model.provider_id ?? null;

                    // Create new productDetail entity and assign it to user entity
                    product.ProductDetail = new ProductDetail()
                    {
                        product_id = product.product_id,
                        description = model.ProductDetail.description ?? null,
                        keyWord = model.ProductDetail.keyWord ?? null,
                        img1 = model.ProductDetail.img1 ?? null,
                        img2 = model.ProductDetail.img2 ?? null,
                        img3 = model.ProductDetail.img3 ?? null,
                        img4 = model.ProductDetail.img4 ?? null,
                        img5 = model.ProductDetail.img5 ?? null,
                        img6 = model.ProductDetail.img6 ?? null
                    };

                    //Add user object into products's EntitySet
                    _context.Products.Add(product);
                    // call SaveChanges method to save product & productDetail into database
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "Thêm mới sản phẩm thành công.";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add product {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi thêm mới sản phẩm.");
                    return View(model);
                }
            }

            ViewData["NewProductId"] = new Guid();
            ViewData["DropdownCat"] = LoadListCategories();
            ViewData["DropdownProvider"] = LoadListProviders();

            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var product = await _context.Products
                                .Include(p=>p.Category)
                                .Include(p=>p.Provider)
                                .Include(p=>p.ProductDetail)
                                .FirstOrDefaultAsync(x => x.product_id.Equals(Id));
            if (product == null)
            {
                ViewData["Errors"] = "Hàng hóa không tồn tại.";
                return RedirectToAction("Index");
            }

            ViewData["DropdownCat"] = LoadListCategories();
            ViewData["DropdownProvider"] = LoadListProviders();

            var model = new ProductViewModel()
            {
                product_id = product.product_id,
                name = product.name,
                quantity = product.quantity,
                price = product.price,
                net_price = product.net_price,
                expiry_date = product.expiry_date,
                user_id = product.user_id,
                status = product.status,
                date_created = product.date_created,
                date_updated = product.date_updated,
                date_deleted = product.date_deleted,
                category_id = product.category_id,
                category_name = product.Category.category_name,
                provider_id = product.provider_id,
                provider_name = product.Provider.provider_name
            };

            model.ProductDetail = new ProductDetailViewModel()
            {
                description = product.ProductDetail.description,
                keyWord = product.ProductDetail.keyWord,
                img1 = product.ProductDetail.img1,
                img2 = product.ProductDetail.img2,
                img3 = product.ProductDetail.img3,
                img4 = product.ProductDetail.img4,
                img5 = product.ProductDetail.img5,
                img6 = product.ProductDetail.img6,
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!model.product_id.HasValue && model.product_id == new Guid())
                    {
                        return View("Index");
                    }

                    var product = await _context.Products.Include(p=>p.ProductDetail).FirstOrDefaultAsync(x => x.product_id.Equals(model.product_id));
                    if (product == null)
                    {
                        TempData["Errors"] = "Hàng hóa không tồn tại trong hệ thống.";
                        return RedirectToAction("Index");
                    }

                    if (product.ProductDetail != null)
                        _context.Entry<ProductDetail>(product.ProductDetail).State = EntityState.Deleted;
                    _context.ProductDetails.Remove(product.ProductDetail);
                    await _context.SaveChangesAsync();

                    // update new product
                    product.name = model.name ?? null;
                    product.quantity = model.quantity ?? null;
                    product.price = model.price;
                    product.net_price = model.net_price;
                    product.expiry_date = model.expiry_date ?? null;
                    product.user_id = AccountId;
                    product.status = model.status;
                    product.date_created = DateTime.Now;
                    product.category_id = model.category_id ?? null;
                    product.provider_id = model.provider_id ?? null;

                    // Create new productDetail entity and assign it to user entity
                    product.ProductDetail = new ProductDetail()
                    {
                        product_id = product.product_id,
                        description = model.ProductDetail.description ?? null,
                        keyWord = model.ProductDetail.keyWord ?? null,
                        img1 = model.ProductDetail.img1 ?? null,
                        img2 = model.ProductDetail.img2 ?? null,
                        img3 = model.ProductDetail.img3 ?? null,
                        img4 = model.ProductDetail.img4 ?? null,
                        img5 = model.ProductDetail.img5 ?? null,
                        img6 = model.ProductDetail.img6 ?? null
                    };

                    //Mark product entity as modified
                    _context.Entry(product).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "Cập nhật thông tin thành công.";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add user {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi cập nhật.");
                    return View(model);
                }
            }
            ViewData["DropdownCat"] = LoadListCategories();
            ViewData["DropdownProvider"] = LoadListProviders();
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return BadRequest();
            }
            var product = await _context.Products.Include(x=>x.OrderProduct).FirstOrDefaultAsync(x => x.product_id.Equals(Id) && x.status == true);
            if (product == null)
            {
                return BadRequest();
            }

            if (product.OrderProduct.Count > 0)
            {
                return Json(0);
            }

            //product.status = false;
            //product.date_deleted = DateTime.Now;
            // xóa hẳn khỏi db
            _context.Products.Remove(product);
            _context.Entry<Product>(product).State = EntityState.Deleted;
            
            await _context.SaveChangesAsync();
            return Json(1);
        }

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(Guid? Id)
        //{
        //    if (!Id.HasValue)
        //    {
        //        TempData["Errors"] = "Mặt hàng không tồn tại hoặc đã bị xóa.";
        //        return RedirectToAction("Index");
        //    }
        //    var product = await _context.Products.FirstOrDefaultAsync(x => x.product_id.Equals(Id) && x.status == true);
        //    if (product == null)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    product.status = false;
        //    _context.Entry<Product>(product).State = EntityState.Modified;
        //    await _context.SaveChangesAsync();
        //    TempData["Success"] = "Xóa hàng hóa thành công.";
        //    return RedirectToAction("Index");
        //}

        public IActionResult GetProduct(string search)
        {
            if (search == null) search = "";
            var data = _context.Products
                               .Include(x=>x.Provider)
                               .Where(x => x.name.Contains(search.Trim().ToLower()))
                               .Select(x => new productSearch {
                                    product_id =  x.product_id,
                                    dvt = "Đơn vị sản phẩm",
                                    net_price = x.net_price,
                                    price = x.price,
                                    product_name = x.name,
                                    provider_name = x.Provider.provider_name
                               }).ToList();
            return Json(data);
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
