﻿using CartShop.Helpers.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;

namespace CartShop.Shop.Controllers.Base
{
    public class BaseController<T> : Controller
    {
        #region Properties

        protected string TokenKey
        {
            get; private set;
        }            

        protected CartShopConfiguration Configuration
        {
            get; private set;
        }   

        protected ILogger Logger
        {
            get; private set;
        }

        private Guid _accountId;
        protected Guid AccountId
        {
            get
            {
                if (_accountId == Guid.Empty)
                {
                    if (HttpContext.User != null
                        && HttpContext.User.FindFirst(ClaimTypes.NameIdentifier) != null)
                    {
                        _accountId = new Guid(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    }
                }
                return _accountId;
            }
        }

        private string _user_name;
        protected string UserNameAcc
        {
            get
            {
                if (_user_name == null)
                {
                    if ((HttpContext.User != null && HttpContext.User.FindFirst(ClaimTypes.Name) != null))
                    {
                        _user_name = HttpContext.User.FindFirst(ClaimTypes.Name).Value;
                    }
                }
                return _user_name;
            }
        }

        private string _emailAccount;
        protected string EmailAccount
        {
            get
            {
                if (_emailAccount == null)
                {
                    if ((HttpContext.User != null && HttpContext.User.FindFirst(ClaimTypes.UserData) != null))
                    {
                        _emailAccount = HttpContext.User.FindFirst(ClaimTypes.UserData).Value;
                    }
                }
                return _emailAccount;
            }
        }

        private string _roleAccount;
        protected string RoleNameAccount
        {
            get
            {
                if (_roleAccount == null)
                {
                    if ((HttpContext.User != null && HttpContext.User.FindFirst(ClaimTypes.Role) != null))
                    {
                        _roleAccount = HttpContext.User.FindFirst(ClaimTypes.Role).Value;
                    }
                }
                return _roleAccount;
            }
        }

        #endregion Properties

        #region Constructors

        public BaseController() { }

        public BaseController(IOptions<CartShopConfiguration> configuration)
        {            
            Configuration = configuration.Value;
        }

        public BaseController(ILoggerFactory loggerFactory, IOptions<CartShopConfiguration> configuration)
        {
            Logger = loggerFactory.CreateLogger<T>();
            Configuration = configuration.Value;
        }        

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            try
            {
                if ((HttpContext.User != null && HttpContext.User.FindFirst(ClaimTypes.UserData) != null))
                {
                    TokenKey = HttpContext.User.FindFirst(ClaimTypes.UserData).Value.GenTokenKey();
                }
               
            }
            catch
            {
                TokenKey = string.Empty;
            }
        }      
        #endregion
    }
}
