﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CartShop.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using CartShop.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CartShop.Shop.Controllers
{
   
    public class CartsController : Base.BaseController<CartsController>
    {
        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public CartsController(
             ILoggerFactory loggerFactory,
             IOptions<CartShopConfiguration> cartShopConfiguration,
             CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<CartsController>();
            _context = context;
        }

        private List<SelectListItem> LoadListCategories()
        {
            // Load List datas
            var datas = _context.Categories.Where(x => x.status == true).ToList();
            var _lstSelect = datas.Select(x => new SelectListItem
            {
                Value = x.category_id.ToString(),
                Text = x.category_name
            }).ToList();
            _lstSelect.Insert(0, new SelectListItem() { Value = "", Text = "--Chọn danh sách--" });
            return _lstSelect;
        }


        // GET: /<controller>/
        [Authorize(Roles = "Administrator, Employee")]
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["OrderCodeSortParm"] = String.IsNullOrEmpty(sortOrder) ? "order_code_desc" : "";
            ViewData["OrderDateSortParm"] = String.IsNullOrEmpty(sortOrder) ? "order_date_desc" : "";
            ViewData["CustomNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "customer_name_desc" : "";
            ViewData["StatusCodeSortParm"] = String.IsNullOrEmpty(sortOrder) ? "status_order_desc" : "";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            // Not display orders deleted and Canceled.
            var datas = _context.Carts
                                .Where(a=>a.status_order == 1 || a.status_order == 3)
                                .Include(a => a.OrderProduct)
                                .Select(a => new CartViewModel
                                {
                                    order_code = a.order_code,
                                    order_date = a.order_date,
                                    customer_name = a.customer_name,
                                    customer_phone = a.customer_phone,
                                    customer_address = a.customer_address,
                                    customer_email = a.customer_email,
                                    note = a.note,
                                    status_order = a.status_order,
                                    user_id = a.user_id,
                                    TenNguoiTao = a.User.user_name,
                                    manhanvien = a.User.Employee.employee_name
                                });
                                

            // check data null
            if (datas == null || datas.Count() == 0)
            {
                return RedirectToAction("Create");
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                // support search with name, email, phone
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                datas = datas.Where(s => s.customer_email.Contains(searchString) || s.customer_name.Contains(searchString) || s.customer_phone.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "order_code_desc":
                    datas = datas.OrderByDescending(s => s. order_code);
                    break;
                case "order_date_desc":
                    datas = datas.OrderByDescending(s => s.order_date);
                    break;
                case "customer_name_desc":
                    datas = datas.OrderByDescending(s => s.customer_name);
                    break;
                case "status_order_desc":
                    datas = datas.OrderByDescending(s => s.status_order);
                    break;
                default:
                    datas = datas.OrderBy(s => s.order_date);
                    break;
            }
            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<CartViewModel>.CreateAsync(datas.AsNoTracking(), page ?? 1, _pageSize));
        }


        [Authorize(Roles = "Administrator, Employee")]
        public IActionResult Create()
        {
            ViewData["NewOrderCode"] = Guid.NewGuid();
            ViewData["DropDownCat"] = LoadListCategories();
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Create(CartViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // create new Cart entity object
                    var cart = new Cart();
                    cart.order_code = model.order_code.Value;
                    cart.order_date = DateTime.Now;
                    cart.customer_name = model.customer_name ?? null;
                    cart.customer_phone = model.customer_phone ?? null;
                    cart.customer_address = model.customer_address ?? null;
                    cart.customer_email = model.customer_email ?? null;
                    cart.note = model.note ?? null;
                    cart.status_order = model.status_order ?? 1;
                    cart.user_id = AccountId;                    

                    // Add OrderProducts
                    if (model.OrderProduct.Count > 0)
                    {
                        //cart.OrderProduct = new List<OrderProduct>();
                        foreach (var order in model.OrderProduct)
                        {
                            var newOrder = new OrderProduct();
                            newOrder.order_code = cart.order_code;
                            newOrder.product_id = order.product_id;
                            newOrder.net_price = order.net_price;
                            newOrder.price = order.price;
                            newOrder.quantity_number = order.quantity_number ?? 1;
                            cart.OrderProduct.Add(newOrder);
                        }
                    }

                    //add before cart entity into Carts entitySet 
                    _context.Carts.Add(cart);
                    // call SaveChanges method to save provider into database
                    await _context.SaveChangesAsync();

                    // Tạo mới và in hóa đơn
                    return Json(1);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add order {ex.ToString()}");
                    //ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi tạo đơn hàng.");
                    return Json(0);
                }
            }
            //ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return Json(0);
        }

        [Authorize(Roles = "Administrator, Employee")]
        public IActionResult Edit(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var cart = _context.Carts
                                .Where(a => a.order_code == Id)
                                .Include(a => a.OrderProduct) 
                                .Select(a => new CartViewModel()
                                {
                                    order_code = a.order_code,
                                    order_date = a.order_date,
                                    customer_email = a.customer_email,
                                    customer_address = a.customer_address,
                                    customer_name = a.customer_name,
                                    customer_phone = a.customer_phone,
                                    status_order = a.status_order,
                                    user_id = AccountId,
                                    OrderProduct = a.OrderProduct.Select(x => new OrderProductViewModel
                                    {
                                        product_id = x.product_id,
                                        product_name = x.Product != null ? x.Product.name : null,
                                        net_price = x.net_price,
                                        price = x.price,
                                        quantity_number = x.quantity_number,
                                        provinder_name = x.Product.Provider.provider_name
                                    }).ToList()
                                }).FirstOrDefault();
            if (cart == null)
            {
                ViewData["Errors"] = "Đơn hàng không tồn tại.";
                return RedirectToAction("Index");
            }           

            return View(cart);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles = "Administrator, Employee")]
        //public async Task<IActionResult> Edit(CartViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            if (!model.order_code.HasValue)
        //            {
        //                return View("Index");
        //            }

        //            var cart = await _context.Carts.FirstOrDefaultAsync(x => x.order_code.Equals(model.order_code));
        //            if (cart == null)
        //            {
        //                TempData["Errors"] = "Đơn hàng không tồn tại trong hệ thống.";
        //                return View("Index");
        //            }

        //            // Chỉ sửa thông tin đơn hàng lại để in hóa đơn, không được sửa chi tiết đơn hàng
        //            cart.order_date = DateTime.Now;
        //            cart.customer_name = model.customer_name ?? null;
        //            cart.customer_phone = model.customer_phone ?? null;
        //            cart.customer_address = model.customer_address ?? null;
        //            cart.customer_email = model.customer_email ?? null;
        //            cart.note = model.note ?? null;
        //            cart.status_order = model.status_order ?? 1;
        //            cart.user_id = AccountId;

        //            //Mark provider entity as modified
        //            _context.Entry(cart).State = EntityState.Modified;
        //            await _context.SaveChangesAsync();

        //            ViewData["Success"] = "Cập nhật thông tin đơn hàng thành công.";
        //            return RedirectToAction("Detail", new { Id = cart.order_code });
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.LogError($"error update cart {ex.ToString()}");
        //            ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi cập nhật.");
        //            return View(model);
        //        }
        //    }
        //    ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
        //    return View(model);
        //}


        [HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Edit(CartViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // update new Cart entity object
                    var cart = await _context.Carts.FindAsync(model.order_code);
                    if (cart == null)
                    {
                        return BadRequest();
                    }


                    cart.order_date = DateTime.Now;
                    cart.customer_name = model.customer_name ?? null;
                    cart.customer_phone = model.customer_phone ?? null;
                    cart.customer_address = model.customer_address ?? null;
                    cart.customer_email = model.customer_email ?? null;
                    cart.note = model.note ?? null;
                    cart.status_order = model.status_order ?? 1;
                    cart.user_id = AccountId;
                    
                    // update OrderProducts
                    if (model.OrderProduct.Count > 0)
                    {
                        var LstOrderProducts = _context.OrderProducts.Where(a => a.order_code == cart.order_code).ToList();

                        // Check orderProduct not exist
                        var notRemoveOrders = LstOrderProducts.Where(t => !model.OrderProduct.Any(o=> t.product_id == o.product_id)).ToList();
                        // delete lst orders removed
                        if (notRemoveOrders.Count > 0)
                        {
                            notRemoveOrders.ForEach(t => {
                                cart.OrderProduct.Remove(t);
                            });
                        }

                        model.OrderProduct.ToList().ForEach(order => {
                            // Check orderProduct exist
                            var existOrder = LstOrderProducts.FirstOrDefault(t => t.order_code == cart.order_code && t.product_id == order.product_id);
                            
                            if (existOrder != null)
                            {
                                existOrder.net_price = order.net_price;
                                existOrder.price = order.price;
                                existOrder.quantity_number = order.quantity_number ?? 1;
                            }
                            else
                            {
                                var newOrder = new OrderProduct();
                                newOrder.order_code = cart.order_code;
                                newOrder.product_id = order.product_id;
                                newOrder.net_price = order.net_price;
                                newOrder.price = order.price;
                                newOrder.quantity_number = order.quantity_number ?? 1;
                                cart.OrderProduct.Add(newOrder);
                            }                            
                        });                       
                    }

                    //add before cart entity into Carts entitySet 
                    _context.Entry<Cart>(cart).State = EntityState.Modified;
                    // call SaveChanges method to save provider into database
                    await _context.SaveChangesAsync();

                    // Tạo mới và in hóa đơn
                    return Json(1);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add order {ex.ToString()}");
                    //ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi tạo đơn hàng.");
                    return BadRequest();
                }
            }
            //ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return Json(0);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(Guid? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            try
            {
                var cart = await _context.Carts.FirstOrDefaultAsync(x => x.order_code.Equals(Id) && x.status_order == 1);
                // status_code: 1-đơn hàng mới, 2-đang chuyển giao,3-đã thanh toán, 4-đã hủy, 5 - đã xóa
                if (cart == null)
                {
                    return Json(0); // 0 = Đơn hàng không tồn tại, đã bị xóa hoặc hủy bỏ.           
                }               

                cart.status_order = 5;
                _context.Entry<Cart>(cart).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(0);
            }           
        }

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(Guid? Id)
        //{
        //    if (!Id.HasValue)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    var cart = await _context.Carts.FirstOrDefaultAsync(x => x.order_code.Equals(Id) && x.status_order == 4);
        //    if (cart == null)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    cart.status_order = 4;
        //    _context.Entry<Cart>(cart).State = EntityState.Modified;
        //    await _context.SaveChangesAsync();
        //    ViewData["Success"] = "Hủy đơn hàng thành công.";
        //    return RedirectToAction("Index");
        //}

        [Authorize(Roles = "Administrator, Employee")]
        public IActionResult Detail(Guid? Id)
        {
            try
            {
                if (!Id.HasValue)
                {
                    return RedirectToAction("Index");
                }

                var datas = _context.Carts
                               .Where(a => a.order_code == Id)
                               .Include(a => a.OrderProduct)                               
                               .Select(a => new CartViewModel() {
                                   order_code = a.order_code,
                                   order_date = a.order_date,
                                   customer_email = a.customer_email,
                                   customer_address = a.customer_address,
                                   customer_name = a.customer_name,
                                   customer_phone = a.customer_phone,
                                   status_order = a.status_order,
                                   user_id = AccountId,
                                   OrderProduct = a.OrderProduct.Select(x => new OrderProductViewModel
                                   {
                                       product_id = x.product_id,
                                       product_name = x.Product != null ? x.Product.name : null,
                                       net_price = x.net_price,
                                       price = x.price,
                                       quantity_number = x.quantity_number
                                   }).ToList()
                               }).FirstOrDefault();                              

               
                return View(datas);

            }
            catch (Exception ex)
            {
                _logger.LogError($"error get cart {ex.ToString()}");
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
