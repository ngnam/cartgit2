﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CartShop.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using CartShop.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CartShop.Shop.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class MenuController : Base.BaseController<MenuController>
    {
        #region variable global
        private readonly CartShopContext _context;
        private readonly ILogger _logger;
        private readonly CartShopConfiguration _cartShopConfiguration;
        #endregion

        public MenuController(
             ILoggerFactory loggerFactory,
             IOptions<CartShopConfiguration> cartShopConfiguration,
             CartShopContext context)
        {
            _cartShopConfiguration = cartShopConfiguration.Value;
            _logger = loggerFactory.CreateLogger<MenuController>();
            _context = context;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["MenuNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "menu_name_desc" : "";
            ViewData["MenuUrlSortParm"] = String.IsNullOrEmpty(sortOrder) ? "menu_url_desc" : "";
            ViewData["LevelUserSortParm"] = String.IsNullOrEmpty(sortOrder) ? "level_user_desc" : "";
            ViewData["OrderBySortParm"] = String.IsNullOrEmpty(sortOrder) ? "order_by_desc" : "";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var datas = from u in _context.Menus
                        select new MenuViewModel
                        {
                           menu_id = u.menu_id,
                           icon = u.icon,
                           level_user = u.level_user,
                           menu_name = u.menu_name,
                           menu_url = u.menu_url,
                           order_by = u.order_by,
                           parent_id = u.parent_id,
                           status = u.status
                        };

            // check data null
            if (datas == null)
            {
                return RedirectToAction("Create");
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.Trim();
                ViewData["CurrentFilter"] = searchString;
                datas = datas.Where(s => s.menu_name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "menu_name_desc":
                    datas = datas.OrderByDescending(s => s.menu_name);
                    break;
                case "menu_url_desc":
                    datas = datas.OrderByDescending(s => s.menu_url);
                    break;
                case "level_user_desc":
                    datas = datas.OrderByDescending(s => s.level_user);
                    break;
                case "order_by_desc":
                    datas = datas.OrderByDescending(s => s.order_by);
                    break;
                default:
                    datas = datas.OrderBy(s => s.menu_name);
                    break;
            }
            int _pageSize = _cartShopConfiguration.pageSize.Value;
            ViewData["PageSize"] = _pageSize;
            return View(await PaginatedList<MenuViewModel>.CreateAsync(datas.AsNoTracking(), page ?? 1, _pageSize));
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MenuViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // create new menu entity object
                    var menu = new Menu();
                    menu.menu_name = model.menu_name ?? null;
                    menu.menu_url = model.menu_url ?? null;
                    menu.icon = model.icon ?? null;
                    menu.level_user = model.level_user ?? null;
                    menu.order_by = model.order_by ?? null;
                    menu.parent_id = model.parent_id ?? null;
                    menu.status = model.status;

                    //Add menu object into Users's EntitySet
                    _context.Menus.Add(menu);
                    // call SaveChanges method to save user & Employee into database
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "Thêm mới menu thành công.";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add menu {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi thêm mới.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        public async Task<IActionResult> Edit(int? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var menu = await _context.Menus.FirstOrDefaultAsync(x => x.menu_id.Equals(Id));
            if (menu == null)
            {
                TempData["Errors"] = "Menu không tồn tại.";
                return RedirectToAction("Index");
            }
           
            var model = new MenuViewModel()
            {
               menu_id = menu.menu_id,
               icon = menu.icon,
               level_user = menu.level_user,
               menu_name = menu.menu_name,
               menu_url = menu.menu_url,
               order_by = menu.order_by,
               parent_id = menu.parent_id,
               status = menu.status
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(MenuViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!model.menu_id.HasValue)
                    {
                        return View("Index");
                    }

                    var menu = await _context.Menus.FirstOrDefaultAsync(x => x.menu_id.Equals(model.menu_id));
                    if (menu == null)
                    {
                        TempData["Errors"] = "Menu không tồn tại trong hệ thống.";
                        return View("Index");
                    }

                    menu.menu_name = model.menu_name ?? null;
                    menu.menu_url = model.menu_url ?? null;
                    menu.icon = model.icon ?? null;
                    menu.level_user = model.level_user ?? null;
                    menu.order_by = model.order_by ?? null;
                    menu.parent_id = model.parent_id ?? null;
                    menu.status = model.status;

                    //Mark menu entity as modified
                    _context.Entry(menu).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "Cập nhật thông tin thành công.";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    _logger.LogError($"error add menu {ex.ToString()}");
                    ModelState.AddModelError(string.Empty, "Có lỗi xảy ra khi cập nhật.");
                    return View(model);
                }
            }
            ModelState.AddModelError(string.Empty, "Vui lòng kiểm tra lại các trường.");
            return View(model);
        }

        public async Task<IActionResult> Delete(int? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var menu = await _context.Menus.FirstOrDefaultAsync(x => x.menu_id.Equals(Id) && x.status == true);
            if (menu == null)
            {
                return RedirectToAction("Index");
            }
            var model = new MenuViewModel()
            {
                menu_id = menu.menu_id,
                menu_name = menu.menu_name
            };
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? Id)
        {
            if (!Id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var menu = await _context.Menus.FirstOrDefaultAsync(x => x.menu_id.Equals(Id) && x.status == true);
            if (menu == null)
            {
                return RedirectToAction("Index");
            }
            menu.status = false;
            _context.Entry<Menu>(menu).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            TempData["Success"] = "Xóa Menu thành công.";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
