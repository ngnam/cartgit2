﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CartShop.Shop.Components.Base
{
    public class BaseViewComponent<T> : ViewComponent
    {

        private string _user_name;
        protected string UserNameAcc
        {
            get
            {
                if (_user_name == null)
                {
                    if ((HttpContext.User != null && HttpContext.User.FindFirst(ClaimTypes.Name) != null))
                    {
                        _user_name = HttpContext.User.FindFirst(ClaimTypes.Name).Value;
                    }
                }
                return _user_name;
            }
        }

        private string _emailAccount;
        protected string EmailAccount
        {
            get
            {
                if (_emailAccount == null)
                {
                    if ((HttpContext.User != null && HttpContext.User.FindFirst(ClaimTypes.UserData) != null))
                    {
                        _emailAccount = HttpContext.User.FindFirst(ClaimTypes.UserData).Value;
                    }
                }
                return _emailAccount;
            }
        }

        private string _roleAccount;
        protected string RoleNameAccount
        {
            get
            {
                if (_roleAccount == null)
                {
                    if ((HttpContext.User != null && HttpContext.User.FindFirst(ClaimTypes.Role) != null))
                    {
                        _roleAccount = HttpContext.User.FindFirst(ClaimTypes.Role).Value;
                    }
                }
                return _roleAccount;
            }
        }
    }
}
