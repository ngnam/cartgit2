﻿using CartShop.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CartShop.Shop.Components
{
    [Authorize]
    public class CurrentUserViewComponent : Base.BaseViewComponent<CurrentUserViewComponent>
    {
        public CurrentUserViewComponent()
        {

        }
        public IViewComponentResult Invoke()
        {
            var model = new CurrentUser()
            {
                UserName = UserNameAcc,
                Email = EmailAccount,
                RoleName = RoleNameAccount
            };
            return View("Default", model);
        }
    }
}
