﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartShop.Shop.Components
{
    public class HelloViewComponent : ViewComponent
    {
        public HelloViewComponent()
        {

        }

        public IViewComponentResult Invoke()
        {
            var model = "xxx";
            return View("Default", model);
        }
       
    }
}
