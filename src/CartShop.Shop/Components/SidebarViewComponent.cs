﻿using CartShop.Models;
using CartShop.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CartShop.Shop.Components
{
    [Authorize]
    public class SidebarViewComponent : ViewComponent
    {
        private readonly CartShopContext _context;
        private List<SidebarViewModel> _lstMenu = new List<SidebarViewModel>() {};
        public SidebarViewComponent(CartShopContext context)
        {
            _context = context;            
        }

        public IViewComponentResult Invoke()
        {
            _lstMenu = GetMenuItems();
            var model = LoadSidebarMenu();
            return View("Default", model);
        }

        private List<SidebarViewModel> LoadSidebarMenu()
        {
            var presidents = _lstMenu.Where(x => x.parent_id == null).ToList();
            foreach (var item in presidents)
            {                
                SetChildren(item, _lstMenu);
            }
           
            return presidents;
        }

        private void SetChildren(SidebarViewModel model, List<SidebarViewModel> danhmuc)
        {
            var childs = danhmuc.Where(x => x.parent_id == model.menu_id).ToList();
            if (childs.Count > 0)
            {
                foreach (var child in childs)
                {
                    SetChildren(child, danhmuc);
                    model.childMenu.Add(child);
                }
            }
        }



        private List<SidebarViewModel> GetMenuItems()
        {
            var model = new List<SidebarViewModel>();
            // Check Role current user
            if (User != null)
            {
                if (User.IsInRole("Administrator"))
                {
                    model = (from m in _context.Menus
                             where m.status == true
                             select new SidebarViewModel()
                             {
                                 menu_id = m.menu_id,
                                 menu_name = m.menu_name,
                                 parent_id = m.parent_id,
                                 menu_url = m.menu_url,
                                 order_by = m.order_by,
                                 icon = m.icon
                             }).OrderBy(x => x.order_by).ToList();
                }
                else if (User.IsInRole("Employee"))
                {
                    model = (from m in _context.Menus
                             where m.status == true && m.level_user == 2 // employee = 2, admin = 1
                             select new SidebarViewModel()
                             {
                                 menu_id = m.menu_id,
                                 menu_name = m.menu_name,
                                 parent_id = m.parent_id,
                                 menu_url = m.menu_url,
                                 order_by = m.order_by,
                                 icon = m.icon
                             }).OrderBy(x => x.order_by).ToList();
                }
            }
            return model;
        }

    }
}
