﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AutoMapper;
using CartShop.Models;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.AspNetCore.Http;

namespace CartShop.Shop
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Adds services required for using options.
            services.AddOptions();

            // Config data Json send to view
            services.AddMvc()
                .AddJsonOptions(opt =>
                {
                    opt.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                })
                .AddViewLocalization()
                .AddDataAnnotationsLocalization();
            
            services.AddLogging();
            services.AddAutoMapper();

            // Add CartShop Configuration
            services.Configure<CartShopConfiguration>(options => Configuration.GetSection(nameof(CartShopConfiguration)).Bind(options));
                       
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddDbContext<CartShopContext>(options => options.UseSqlServer(Configuration.GetConnectionString("CartShopDbConnection"), b => b.UseRowNumberForPaging()));

            // Add Cookie
            services.AddDistributedMemoryCache();
            services.AddMemoryCache();
            services.AddSession(options =>
            {
                options.CookieName = ".CartShop.Session";
                int timeoutInSeconds = 7200; // 2 hours
                var timeoutVal = Configuration.GetSection("CartShopConfiguration").GetValue<string>("IdleTimeout");
                if (!int.TryParse(timeoutVal, out timeoutInSeconds))
                {
                    timeoutInSeconds = 7200; // 2 hours
                }
                options.IdleTimeout = TimeSpan.FromSeconds(timeoutInSeconds);
            });

            // Add application services.
            //services.AddTransient<IProductsServices, ProductsServices>();
            //services.AddTransient<ISmsSender, AuthMessageSender>();
            //services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseApplicationInsightsRequestTelemetry();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // Config Session
            app.UseSession();
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                CookieName = ".CartShop.Admin.User",
                LoginPath = new PathString("/Account/Login"),
                LogoutPath = new PathString("/Account/Logout"),
                ExpireTimeSpan = TimeSpan.FromSeconds(7200),
                AccessDeniedPath = new PathString("/Account/AccessDenied")
            });          


            app.UseApplicationInsightsExceptionTelemetry();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
