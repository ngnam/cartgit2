﻿using CartShop.Helpers.Extensions;

namespace CartShop.Shop
{
    public class CartShopConfiguration
    {
        public CartShopConfiguration()
        {
            pageSize = 25;
        }

        public int? pageSize { get; set; }
        public int IdleTimeout {get; set;}
        public string _baseUrl { get; set; }
        public string baseUrl
        {
            get { return _baseUrl; }
            set
            {
                _baseUrl = value;
                _baseUrl = _baseUrl.NormalizeUri();
            }
        }
    }
}
